console.log('directives.js start load');

// Generates the listing panel
app.directive('gkListings', ['$http', '$document', function ($http, $document) {

    return {
        restrict: 'E',
        templateUrl: 'listings.html',
        scope: {
            ui: '=',
            acc: '=',
            listings: '=',
            listingPanels: '=', // with the layout etc...
            methods: '=',
            queryParams: '=',
            static: '=',

            // options
            showSortOrder: '=',
            loadMore: '='
        },
        link: function (scope, element, attr) {
            
            scope.allListingsFetched = false;
            scope.gui = {
                controls: {}
            };
            scope.fx = {};
            scope.searchTerms = []; // This holds the display values of queryParams
            
            // init
            // we're only using this variable to tell us when to load the iframe. 
            // if we're not using iframes then get rid of this variable and its dependencies
            scope.ui.listingPanelTransitionComplete = true;
            scope.gui.controls.showDetailedPanel = function (listing) {
                
                console.log(listing);
                
                scope.gui.currSelectedGalleryLeftPosition = 0;

                // dont allow simultaneous transitions
                if (!scope.ui.listingPanelTransitionComplete) {
                    return;
                }
                scope.ui.listingPanelTransitionComplete = false;

                var listingIndex = findIndexById(scope.listings, listing.id);
                if (listingIndex < 0) {
                    scope.ui.listingPanelTransitionComplete = true;
                    return;
                }

                
                // check for toggle
                if (listingIndex == scope.ui.currSelectedPanelId) {

                    // toggle off
                    scope.ui.currSelectedPanelId = -1;
                    scope.ui.currDetailedPanelId = -1;
                    scope.ui.listingPanelTransitionComplete = true;
                }
                else {
                    scope.ui.currSelectedImageIndex = 0;
                    scope.ui.currSelectedPanelId = listingIndex;

                    // reset the gallery
                    scope.ui.galleryIndex = 0;

                    var detailedPanelIndex = Math.floor(listingIndex / scope.ui.nCol);
                    if (detailedPanelIndex != scope.ui.currDetailedPanelId) {
                        scope.ui.currDetailedPanelId = detailedPanelIndex;
                    }

                    // scrolling effect
                    if (scope.ui.currDetailedPanelId >= 0) {
                        scope.gotoElement('listing-bottom-' + listing.id)
                    }

                    // deliberately delay loading of google maps - to allow smooth transition
                    setTimeout(function () {
                        scope.ui.listingPanelTransitionComplete = true;
                        scope.$apply();
                    }, 2000)
                }
            }
            // Smooth scrolling
            scope.gotoElement = function (eID) {

                // wait for completion before starting animation?

                setTimeout(function () {
                    var e = angular.element(document.getElementById(eID));
                    $document.scrollToElement(e, 60, 1000);
                }, 0)
            };

            scope.getArray = function(n) {
                return new Array(n);
            }

            scope.loadDefaultGoogleMap = function (listing) {
                listing.mapSrc = scope.methods.getGoogleMapSrc(listing.address);
            }

            // Get more
            if (scope.loadMore) {

                var requestPending;
                $document.bind('scroll', function () {
    
                    if (scope.ui.currMainDisplay != 'listings') {
                        return;
                    }
                    if (requestPending || scope.allListingsFetched) {
                        return;
                    }
    
                    var body = document.body, html = document.documentElement;
                    var totalHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
                    var pageHeight = (document.height !== undefined) ? document.height : body.offsetHeight;
    
                    if (window.scrollY + pageHeight >= 0.9 * totalHeight) {
                        requestPending = true;
                        scope.sendMoreRequest();
                    }
                    scope.$apply();
                });
            }
            scope.sendMoreRequest = function () {

                if (scope.queryParams == undefined) {
                    console.log('warning: undefined query params');
                }
                
                // sp's modified server side
                $http.post('/search', scope.queryParams).then(
                    function success(res) {

                        scope.ui.displayListings(res.data);

                        if ((res.data.listings == null || res.data.listings.length == 0) &&
                            (res.data.extralistings == null || res.data.extralistings.length == 0)) {
                            scope.allListingsFetched = true;
                            return;
                        }
                        requestPending = false;
                    },
                    function error(err) {
                        console.log(err);
                    }
                )
            }

            scope.onReorderRequest = function (order) {

                if (scope.queryParams == undefined) {
                    return;
                }

                scope.queryParams.orderby = order;
                scope.queryParams.from = 0;
                scope.queryParams.extrafrom = 0;

                // Remove from view
                scope.listings = [];
                scope.listingPanels = [];

                $http.post('/search', scope.queryParams).then(
                    function success (res) {

                        scope.ui.displayListings(res.data, true);
                        // This is a hack to re-bind the parent variable 
                        scope.listings = scope.$parent.$parent.listings;
                        
                        if ((res.data.listings == null || res.data.listings.length == 0) &&
                            (res.data.extralistings == null || res.data.extralistings.length == 0)) {
                            scope.allListingsFetched = true;
                            return;
                        }
                    },
                    function error (err) {
                        console.log(err);
                    }
                )
            }

            scope.toCapitalCase = function (str) {
                return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
            }
        }
    };
}]);

app.directive('gkGallery', function () {
    return {
        restrict: 'E',
        templateUrl: 'gallery.html',

        scope: {
            images: '=',
            ui: '=',
            gui: '=',
            resize: '='
        },

        link: function (scope, element) {

            scope.shiftGalleryImage = function (ch) {
                var nImages = scope.images.length;
                scope.ui.galleryIndex = (scope.ui.galleryIndex + ch + nImages) % nImages;
            }
            scope.shiftPreviewPanel = function (ch) {

                //var width = document.getElementById('img-panel-top-preview').clientWidth;//this returns 0?
                var maxStep = 20 * ch;
                var frames = 30;
                var time = 400;
                var timeStep = time / frames;

                // Animate!
                for (var i = 1; i <= frames; i++) {
                    var step = -(1 - i / frames) * maxStep;
                    setTimeout(function (step) {
                        var newLocation = scope.gui.currSelectedGalleryLeftPosition + step;
                        if (newLocation > 0) newLocation = 0;

                        //if (-newLocation > width) newLocation = -width;

                        // conditionals here
                        scope.gui.currSelectedGalleryLeftPosition = newLocation;
                        scope.$apply();
                    }, timeStep * i, step);
                }
            }

            scope.maxWidth = undefined;
            if (scope.resize) {
                scope.$watch(
                    function () { 
                        return {
                           width: element.parent()[0].clientWidth,
                           height: element.parent()[0].clientHeight,
                        }
                   },
                   function (size) {
                       if (size != undefined) {
                        scope.maxWidth = Math.min(size.width, (size.height - 50) / 0.75) + 'px';
                       }
                   },
                   true
                );
            }
        }
    }
});

console.log('directives.js end load');
