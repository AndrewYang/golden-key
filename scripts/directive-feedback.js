app.directive('gkFeedback', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'feedback.html',
        scope: {
            ui: '=',
            acc: '='
        },
        link: function(scope, element, attrs) {

            scope.formSubmitted = false;
            scope.formContent = {};
            scope.hiddenContent = {};
            
            scope.submitForm = function(route, feedback) {

                if (feedback.email == null) {
                    feedback.result = {
                        en: 'Please enter your email address.',
                        ch: '请输入您的电子邮件地址。'
                    };
                    return;
                }
                if(!(/(.+)@(.+){2,}\.(.+){2,}/.test(feedback.email))) {
                    feedback.result = {
                        en: 'Please enter a valid email address.',
                        ch: '请输入有效的电子邮件地址。'
                    }
                    return;
                }
                if (feedback.comment == null) {
                    feedback.result = {
                        en: 'Please enter some comments to help us improve our services.',
                        ch: '请写一些评论。'
                    }
                    return;
                }

                $http.post(
                    '/feedback' , 
                    { e: feedback.email, r: route, c: feedback.comment }
                ).then(
                    function success() {
                        scope.formSubmitted = true;
                        console.log('setting formSubmitted to true');
                    },
                    function error() {
                        feedback.result = {
                            en: 'Unfortunately, something has gone wrong. Please try again later.',
                            ch: '不幸的是，出了点问题。请稍后再试。'
                        }
                    }
                )
            }

            scope.clearForm = function () {
                scope.formSubmitted = false;
            }
        }
    }
});