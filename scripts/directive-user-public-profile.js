app.directive('gkUserPublicProfile', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'userPublicProfile.html',
        scope: {
            listing: '=',
            acc: '=',
            ui: '='
        },
        link: function(scope, element, attrs) {

            scope.initMessage = function() {

                // Set up new message for currently selected listing and (agent) user id

                // Check for token expiry
                if (scope.acc == null) {
                    scope.sendMsg.result = 'Please log in or sign up to continue'
                }
            }
        }
    }
});