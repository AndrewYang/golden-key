app.directive('gkHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'head.html',
        
        link: function(scope, element, attrs) {
            
            // Default is no menu is showing
            scope.hui = {
                showDropdownMenu: false,
                layoutCutoff: 900,
                smallHead: false
            }

            // This is very similar to ui.isMobileDisplay, except with a different cutoff
            scope.recalculateSmallHead = function (width) {
                if (scope.ui.isMobileDevice) return true;
                return width <= scope.hui.layoutCutoff;
            }

            scope.$watch('ui.size.width', function (newVal, oldVal) {
                if (newVal != oldVal && !isNaN(newVal)) {
                    scope.hui.smallHead = scope.recalculateSmallHead(newVal);
                }
            });
            scope.togSearch = function(isShow) {
                if (scope.ui.isMobileDisplay()) {
                    if (isShow) {
                        scope.showMobileSearchPanel();
                    }
                    else {
                        scope.hideMobileSearchPanel();
                    }
                }
            }
            scope.setLang = function(isEn) {
                scope.ui.en = isEn;
                document.cookie = "lang=" + (isEn ? 'en' : 'ch');
            }

            // on load 
            var dim = getWindowSize();
            if (dim != undefined) {
                scope.hui.smallHead = scope.recalculateSmallHead(dim.width);
            }
        }
    }
});