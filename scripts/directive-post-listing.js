app.directive('gkPostListing', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'postListing.html',

        scope: {
            ui: '=',
            acc: '=',
            static: '=',
            methods: '=',
            newlist: '='
        },
        link: function (scope, element, attrs) {

            scope.features = undefined;
            scope.inspectionTimeOptions = ['A.M.', 'P.M.'];
            scope.currInspection = {
                dateObject: undefined
            };
            scope.displayForm = false;

            // There is a duplicate in directives.js
            scope.getListingTypeDisplay = function (type) {
                if (type == undefined) return '';

                if (type.k == 'Sell') {
                    return scope.ui.en ? 'For sale' : '出售';
                }
                if (type.k == 'Lease') {
                    return scope.ui.en ? 'For lease' : '出租';
                }
                if (type.k == 'Sold') {
                    return scope.ui.en ? 'Sold' : '已售';
                }
                if (type.k == 'Under offer') {
                    return scope.ui.en ? 'Under Offer' : '低于报价';
                }
                return type.k;
            }
            scope.getActualStates = function () {
                var states = [];
                for (var i = 0; i < scope.static.states.length; i++) {
                    var s = scope.static.states[i];
                    if (s.en != 'All') states.push(s);
                }
                return states;
            };
            
            scope.submitLink = function(link) {

                // Remove error message, if it exists
                scope.newlist.result1clickListing = undefined;

                scope.loading1clickListing = true; // non-loading bar ui functions
                scope.progressBar = {
                    show: true,
                    description: 'Loading content'
                }
                scope.progressBar.state = 0; // force reset
                
                $http.post('/oneclicklisting', { l: link }).then(
                    function success(res) {
                        
                        console.log(res.data);

                        var listing = res.data.listing;
                        var l = scope.ui.populateListingForm(listing, 'new');

                        if (listing.imageIds != undefined) {
                            scope.loadOffsiteImages(listing.imageIds, 0);
                        }
                        else {
                            scope.loading1clickListing = false;         // conclude
                            scope.progressBar.state = 100;
                            scope.progressBar.description = 'Finished';
                        }
                    },
                    function error(err) {
                        console.log(err);
                        scope.loading1clickListing = false;     // conclude
                        scope.progressBar.show = false;
                        scope.progressBar.state = 0;

                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }

                        scope.newlist.result1clickListing = 
                                'Unfortunately, we are unable to obtain the required information from this link.\n\n' +
                                'Tip: Often it helps if you are logged out of the website before copying and pasting the link. If this does not work, please contact us for assistance.'
                    }
                )
            };
            scope.loadOffsiteImages = function(images, index) {
                if (images == undefined || index < 0 || index >= images.length) {
                    scope.loading1clickListing = false; // conclude
                    scope.progressBar.state = 100;
                    scope.progressBar.description = 'Finished';
                    scope.displayForm = true;
                    return;
                }

                scope.progressBar.state = 100 * (1 + index) / (images.length + 1);
                scope.progressBar.description = 'Loading image ' + (index + 1) + ' of ' + images.length;

                // Image src invalid, skip to the next one
                if (images[index].src == undefined || images[index].src.length == 0) {
                    scope.loadImages(images, index + 1);
                    return;
                }

                $http.post('/oneclicklistingimage', { url: images[index].src }).then(
                    function success (res) {
                        // The response is just a byte[] - only the client has it! This is not stored on the server until listing submission
                        var file = scope.newlist.fileread[index];
                        file.origin = 'c';
                        file.result = "data:image/png;base64," + res.data;
                        scope.loadOffsiteImages(images, index + 1);
                    },
                    function error (err) {
                        scope.loadOffsiteImages(images, index + 1);
                    }
                )
            }

            scope.$watch('newlist.address.chosenSuburb', function () {
                var address = scope.newlist.address;
                if (address.chosenSuburb != undefined) {
                    address.suburb = address.chosenSuburb.s;
                    address.postcode = address.chosenSuburb.p;
                    
                    console.log('need to set state as: ' + address.chosenSuburb.t);

                    // match the state
                    for (var i = 0; i < scope.static.states.length; i++) {
                        if (scope.static.states[i].en == address.chosenSuburb.t) {
                            address.state = scope.static.states[i].en;
                        }
                    }
                }
            });
            scope.getPriceDescriptionTranslation = function(listing) {

                // client side service only - server side code separate
                switch (listing.pricedescriptiontype) {
                    case 'Fixed price': return 'A$' + formatPrice(listing.pricedescription);
                    case 'Auction': return 'Auction';
                    case 'Contact_agent': return 'Contact agent for price';
                    case 'All_offers': return 'All offers welcome';
                    case 'Between': return 'From A$' + formatPrice(listing.pricedescription1) + ' to A$' + formatPrice(listing.pricedescription2);
                    case 'From': 
                    case 'Over':
                    case 'Offers from':

                    case 'Low':
                    case 'Mid':
                    case 'High':
                        return 'About A$' + formatApproxPrice(listing.pricedescription, listing.pricedescriptiontype);
                }
                return '';
            }

            /* http method for getting all features */
            scope.ui.shown.postListingFeatures = false;
            scope.toggleGetFeatures = function () {

                scope.ui.shown.postListingFeatures = !scope.ui.shown.postListingFeatures;
                if (!scope.ui.shown.postListingFeatures) return;

                if (scope.acc == undefined) {
                    scope.ui.onInactivityLogout();
                    return;
                }

                // cache features
                if (scope.features != undefined) {
                    return;
                }

                $http.get('/features', {}).then(
                    function success (res) {

                        console.log(res);

                        // group features by category
                        scope.features = {
                            property: [],
                            rooms: [],
                            location: [],
                            extras: []
                        }
                        
                        var features = res.data.features;
                        for (var i = 0; i < features.length; i++) {
                            switch (features[i].category) {
                                case 'Features': 
                                    scope.features.property.push(features[i]); 
                                    break;

                                case 'Rooms': 
                                    scope.features.rooms.push(features[i]); 
                                    break;

                                case 'Location':
                                    scope.features.location.push(features[i]);
                                    break;

                                case 'Extras': 
                                default:
                                    scope.features.extras.push(features[i]); 
                                    break;
                            }
                        }
                    },
                    function error (err) {
                        console.log(err);
                    }
                )
            }

            // 2-way binding for calendar and input box
            scope.$watch('currInspection.dateObject', function (newVal, oldVal) {
                if (newVal == undefined) return;
                if (!oldVal || (newVal.getTime() !== oldVal.getTime())) {

                    var day = newVal.getDate().toString();
                    if (day.length == 1) day = '0' + day;

                    var month = (newVal.getMonth() + 1).toString();
                    if (month.length == 1) month = '0' + month;

                    scope.currInspection.date = day + '/' + month + '/' + newVal.getFullYear();
                }
            });
            scope.pushInspectionTime = function (c) {

                if (scope.newlist.inspectiontimes == undefined) {
                    scope.newlist.inspectiontimes = [];
                }

                c.errMsg = '';
                var date = parseDate(c.date);
                if (isNaN(date)) {
                    c.errMsg = 'Please enter a valid date.';
                    return;
                }
                var startTime = parseTime(c.start, c.startampm);
                if (isNaN(startTime)) {
                    c.errMsg = 'Please enter a valid start time.';
                    return;
                }
                var endTime = parseTime(c.end, c.endampm);
                if (isNaN(endTime)) {
                    c.errMsg = 'Please enter a valid finish time.';
                    return;
                }
                
                scope.newlist.inspectiontimes.push({
                    display: formatDateTimeInterval(date + startTime, date + endTime, 'en-AU', false),
                    start: date + startTime,
                    end: date + endTime
                });
                
                // clear existing fields
                c.date = undefined;
                c.start = undefined;
                c.end = undefined;
                c.startampm = scope.inspectionTimeOptions[0];
                c.endampm = scope.inspectionTimeOptions[0];
            }

            /* Checking methods for progress */
            scope.complete = {};
            scope.$watch('[newlist.address.street, newlist.address.suburb, newlist.address.state, newlist.address.postcode]', 
                function () { 
                    scope.complete.address = userCompletedAddress(scope.newlist);
                },
                true);
                
            scope.$watch('[newlist.pricedescriptiontype, newlist.pricedescription, newlist.pricedescription1, newlist.pricedescription2]',
                function () {
                    scope.complete.price = completedPriceDescription(scope.newlist);
                    if (scope.complete.price) {
                        scope.priceTranslation = scope.getPriceDescriptionTranslation(scope.newlist);
                    }
                    else {
                        scope.priceTranslation = '';
                    }
                },
                true);
            
            scope.$watch("[newlist.features['Bedrooms'], newlist.features['Bathrooms'], newlist.features['Garages']]", 
                function() {
                    scope.complete.homeFeatures = completedFeatures(scope.newlist);
                },
                true);

            scope.$watch('[newlist.title, newlist.description]',
                function() {
                    scope.complete.description = completedDescription(scope.newlist);
                },
                true);

            /* Submission methods  */
            scope.submitListing = function (listing) {

                if (scope.acc == null) {
                    scope.ui.onInactivityLogout();
                    return;
                }
                if (listing == null) {
                    scope.newlist.result = {
                        en: "Please fill in the form before submitting your listing.",
                        ch: '请在提交商家信息之前填写表格。'
                    }
                    return;
                };

                if (!completedPriceDescription(listing)) {
                    scope.newlist.result = {
                        en: 'Please provide a price description.',
                        ch: '请提供价格说明。'
                    }
                    return;
                };
                if (!userCompletedAddress(listing)) {
                    scope.newlist.result = {
                        en: "Please finish filling in the address for this listing.",
                        ch: '请提供您的商家信息的地址。'
                    };
                    return;
                };
                if (!completedFeatures(listing)) {
                    scope.newlist.result = {
                        en: 'Please complete the home features section.',
                        ch: '请填写主页功能部分'
                    };
                    return;
                };
                if (!listing.selecteduser == undefined) {
                    scope.newlist.result = {
                        en: 'Please choose a user profile for this listing.',
                        ch: '请为此商家信息选择一个用户个人资料。'
                    };
                    return;
                };
                if (!listing.useragreementchecked) {
                    scope.newlist.result = {
                        en: 'Please agree to the terms and conditions of using mfmf.com.au prior to submitting your listing.',
                        ch: '在提交商家信息之前，请同意使用 mfmf.com.au 的条款和条件。'
                    };
                    return;
                };
                
                // Check if edit or new post
                if (scope.newlist.userMode == 'edit') {
                    scope.sendListingData('/editlisting', listing);
                }
                else {
                    scope.sendListingData('/listings', listing);
                }
            };
            scope.sendListingData = function(route, listing) {

                // ALready sending data
                if (scope.sendingListingData) {
                    console.log('we are already sending your data');
                    return;
                }

                scope.sendingListingData = true;
                scope.submitProgress = {
                    show: true,
                    description: 'Uploading your listing',
                    state: 0
                };

                // we need to delete images from the server 
                var deleteImgIds = [];
                if (route == '/editlisting') {
                    var files = scope.newlist.fileread;
                    for (var f = 0; f < files.length; f++) {
                        if (files[f].origin == 's' && files[f].isReplaced) {
                            deleteImgIds.push(files[f].id);
                        }
                    }
                }

                var q = {
                    listingId: listing.id,
                    accountId: scope.acc.id,

                    ltype: listing.ltype.k, // listing type is being stored as the obj
                    type: listing.type,
                    price: listing.price,
                    pricedescription: {
                        type: listing.pricedescriptiontype,
                        price: listing.pricedescription,
                        minprice: listing.pricedescription1,
                        maxprice: listing.pricedescription2
                    },
                    address: listing.address,

                    features: listing.features,

                    title: listing.title,
                    description: listing.description,

                    inspectiontimes: listing.inspectiontimes,
                    deleteImgs: deleteImgIds,

                    agencyid: listing.selectedAgency == undefined ? -1 : listing.selectedAgency.id,
                    userid: listing.selecteduser.id
                };

                $http.post(route, q).then(
                    function success(res) {

                        console.log('posting listing success:\t' + JSON.stringify(res));

                        // obtain listing id first, then attach to all images
                        var listingId = res.data.listingId;
                        if (listingId == null) {
                            scope.showSendResult("N/A", 0, 0);  // no listing id
                            scope.sendingListingData = false;
                            return;
                        }

                        var filesToSend = [];
                        for (var f = 0; f < scope.newlist.fileread.length; f++) {
                            var file = scope.newlist.fileread[f];
                            if (file.origin == 'c' || (file.origin == 's' && file.isReplaced)) {
                                filesToSend.push(file.result);
                            }
                        }
                        scope.sendImageFiles(listingId, filesToSend, 0, 0);
                    },
                    function error(err) {
                        
                        scope.sendingListingData = false;
                        
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                        
                        scope.newlist.result = {
                            en: 'Unable to post this listing.',
                            ch: '不幸的是，发生了错误。请再试一次。' // generic chinese message?
                        }

                        scope.submitProgress.description = "Unfortunately, we encountered an error.";
                        scope.submitProgress.state = 0;
                        scope.submitProgress.show = false;
                    }
                )
            }
            scope.sendImageFiles = function(listingId, images, index, nSuccess) {

                // finished
                if (images.length <= 0 || index >= images.length) {
                    scope.showSendResult(listingId, images.length, images.length - nSuccess);
                    scope.sendingListingData = false;
                    return;
                }

                // Update progress description
                scope.submitProgress.description = 'Uploading image ' + (index + 1) + ' of ' + images.length;

                var image = images[index];
                var fd = new FormData();
                var imgBlob = dataURItoBlob(image);
                fd.append('file', imgBlob);
                fd.append('listingId', listingId);
                fd.append('accountId', scope.acc.id);
                fd.append('filetype', 'listingImg');

                $http.post(
                    '/files',
                    fd,
                    {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        timeout: 30000
                    }
                ).then(
                    function success(res) {
                        scope.submitProgress.state = 100 * (index + 2) / (images.length + 1);
                        scope.sendImageFiles(listingId, images, index + 1, nSuccess + 1);
                    },
                    function error(res) {
                        if (res.status == 413) {
                            scope.ui.addNotif({
                                en: 'The image is too big. we will keep uploading your other images in the meantime.',
                                ch: '您上传的图片太大了。我们会继续上传您的其他图片。',
                                icon: 'em'
                            });
                        }
                        scope.submitProgress.state = 100 * (index + 2) / (images.length + 1);
                        scope.sendImageFiles(listingId, images, index + 1, nSuccess);
                    }
                )
            };
            scope.showSendResult = function (listingId, nImages, nFailed) {
                // Reset page 
                scope.submitProgress.show = false;

                scope.newlist = {};
                scope.newlist.result = {
                    iscomplete: true,
                    totalImg: nImages,
                    failedImg: nFailed,
                    listingId: listingId
                };
            }
            scope.userInitiatedAddress = function(listing) {
                if (listing == undefined || listing.address == undefined) return false;
                return isValidStr(listing.address.street) || isValidStr(listing.address.suburb) || listing.address.postcode != null;
            }
            scope.searchGoogleMaps = function(listing) {

                listing.address.mapSaved = false;

                if (!userCompletedAddress(listing)) {
                    listing.address.msg = 'Please fill out the street address, suburb, state and postcode.';
                    return;
                }

                var address = listing.address.street + ',' + listing.address.suburb + ' ' + listing.address.state + ' ' + listing.address.postcode;
                var src = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address.replace(' ', '+') + '&key=' + scope.static.googleMapsApiKey;

                $http.get(src).then(
                    function success (res) {
                        var candidates = res.data.results;
                        if (candidates.length > 0) {
                            var a = res.data.results[0];
                            if (a.geometry != undefined && a.geometry.location != undefined) {
                                listing.address.lat = a.geometry.location.lat;
                                listing.address.lng = a.geometry.location.lng;
                                listing.address.mapSrc = 'https://www.google.com/maps/embed/v1/place?key=' + scope.static.googleMapsApiKey + 
                                    '&q=location=' + a.geometry.location.lat + ',' + a.geometry.location.lng;
                            }
                        }
                    },
                    function error (err) {
                        listing.address.msg = 'Unfortunately, we are unable to find location "' + address + '". Please try a different address';
                    }
                )
            }
        }
    }
});
function userCompletedAddress (listing) {

    if (listing == undefined || listing.address == undefined) {
        return false;
    }
    return isValidStr(listing.address.street) &&
            isValidStr(listing.address.suburb) &&
            listing.address.postcode != null &&
            isValidStr(listing.address.state);
}
function completedPriceDescription (listing) {

    if (listing.pricedescriptiontype == null) return false;

    if (listing.pricedescriptiontype == 'Between') {
        return listing.pricedescription1 != null && listing.pricedescription2 != null;
    }
    
    // other wise, we only need once price (for auctions it is the also the indicative price)
    return listing.pricedescription != null;
}
function completedFeatures (listing) {

    if (listing == undefined || listing.features == undefined) return false;
    
    return listing.features['Bedrooms'] != undefined &&
        listing.features['Bathrooms'] != undefined &&
        listing.features['Garages'] != undefined;
}
function completedDescription (listing) {
    if (listing == undefined) return false;
    return isValidStr(listing.title) && isValidStr(listing.description);
}
function isValidStr(entry) {
    return entry != undefined && entry != null && entry.length > 0;
}
function formatPrice (price, roundThousands) {
    if (roundThousands) {
        return (Math.round(price/1000)*1000).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return price.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function formatApproxPrice (price, level) {

    var strValue = price.toFixed(0);
    if (strValue.length == 6) {
        var bottomPrice = Math.floor(price / 100000) * 100000;
        if (level == 'Low') return formatPrice(bottomPrice + 25000);
        if (level == 'High') return formatPrice(bottomPrice + 75000);
        return formatPrice(bottomPrice + 50000);
    }
    if (strValue.length == 7) {
        // E.g. Mid 1,100,000
        if (endsWith(strValue, '00000') && !endsWith(strValue, '000000')) {
            var bottomPrice = Math.floor(price / 100000) * 100000;
            if (level == 'Low') return formatPrice(bottomPrice + 25000);
            if (level == 'High') return formatPrice(bottomPrice + 75000);
            return formatPrice(bottomPrice + 50000);
        }
        // E.g. Mid 10,000,000
        var bPrice = Math.floor(price / 1000000) * 1000000;
        if (level == 'Low') return formatPrice(bPrice + 250000);
        if (level == 'High') return formatPrice(bPrice + 750000);
        return formatPrice(bPrice + 500000);
    }

    return formatPrice(price);
}
function endsWith(string, target) {
    return string.substr(-target.length) === target;
}

function parseDate (date) {
    var parts = date.split('/');
    if (parts.length == 3) {

        if (parts[0].length == 1) parts[0] = '0' + parts[0];
        else if (parts[0].length > 2) parts[0] = parts[0].substr(parts.length - 2);

        if (parts[1].length == 1) parts[1] = '0' + parts[1];
        else if (parts[1].length > 2) parts[1] = parts[1].substr(parts.length - 2);

        if (parts[2].length == 2) parts[2] = '20' + parts[2];
        
        var str = parts[1] + '/' + parts[0] + '/' + parts[2];
        return Date.parse(str);
    }
    return NaN;
}
function parseTime (time, ampm) {

    if (time == undefined || time.length == 0) return NaN;

    // Only hh:mm, hh;mm and hh.mm are acceptable 

    var parts = [];
    if (time.indexOf(":") >= 0) {
        parts = time.split(":");
    }
    else if (time.indexOf(";") >= 0) {
        parts = time.split(";");
    }
    else if (time.indexOf(".") >= 0) {
        parts = time.split(".");
    }
    else if (time.indexOf(",") >= 0) {
        parts = time.split(",");
    }
    else {
        parts = [time, '00'];
    }

    if (parts.length !== 2) return NaN;
    try {
        var hour = parseInt(parts[0]);
        hour = hour % 12;
        if (hour < 0) return NaN;
        
        var minute = parseInt(parts[1]);
        minute = minute % 60;
        if (minute < 0) return NaN;

        if (ampm == 'P.M.') {
            return ((12 + hour) * 60 + minute) * 60 * 1000;
        }
        return (hour * 60 + minute) * 60 * 1000;
    }
    catch (error) {
        return NaN;
    }
}