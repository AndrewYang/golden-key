console.log('mainCtrl.js start load');

app.controller("mainCtrl", function mainController($scope, $http, $timeout, $sce, $window) {

    $scope.static = {
        states: [
            { en: 'All', ch: '所有' },
            { en: 'WA', ch: '西澳洲' },
            { en: 'NSW', ch: '新南威爾士州' },
            { en: 'VIC', ch: '維多利亞州' },
            { en: 'QLD', ch: '昆士蘭州' },
            { en: 'SA', ch: '南澳洲' },
            { en: 'NT', ch: '北領地' },
            { en: 'TAS', ch: '塔斯曼尼亞州' },
        ],
        propertyTypes: [
            { en: 'Any', ch: '所有' },
            { en: 'House', ch: '独栋别墅' },
            { en: 'Townhouse', ch: '联排别墅' },
            { en: 'Villa', ch: '共院房屋' },
            { en: 'Apartment or Unit', ch: '公寓和单元房' },
            { en: 'Land', ch: '空地' },
            { en: 'Rural', ch: '庄园和农场' },
            { en: 'Retirement living', ch: '老年公寓' }
        ],
        sortTypes: [
            { en: 'Most relevant', ch: '最相关' },
            { en: 'Newest to oldest', ch: '从最新到最旧' },
            { en: 'Oldest to newest', ch: '从最旧到最新' },
            { en: 'Price - lowest to highest', ch: '价格（最便宜到最贵)' },
            { en: 'Price - highest to lowest', ch: '价格（最贵到最便宜)' },
        ],
        messageTypes: [
            { en: 'Expression of interest', ch: '表达兴趣' },
            { en: 'Price information', ch: '价格信息' },
            { en: 'Scheduling an inspection', ch: '预约看房' },
            { en: 'Rates and fees', ch: '价格和费用' },
            { en: 'General inquiries', ch: '一般查询' }
        ],
        priceDescriptionTypes: [
            { en: 'Fixed price', ch: '固定价格' },
            { en: 'Auction', ch: '拍卖' },
            { en: 'Between', ch: '之间' },
            { en: 'From', ch: '从' },
            { en: 'Over', ch: '过度' },
            { en: 'Offers from', ch: '优惠' },
            { en: 'Low', ch: '低位价' },
            { en: 'Mid', ch: '中位价' },
            { en: 'High', ch: '高位价' },
            { en: 'Contact agent', ch: '联系代理商的价格' },
            { en: 'All offers', ch: '所有优惠均已接受' }
        ],
        listingTypes: [
            { en: 'Buy', ch: '买房', k: 'Sell', show: true },
            { en: 'Rent', ch: '租房', k: 'Lease', show: true },
            { en: 'Sold', ch: '已售', k: 'Sold', show: true },
            { en: 'Under offer', ch: '低于报价', k: 'Under offer', show: false },
        ],
        accountTypes: [
            { t: 'a', en: 'Home seller/Real estate agent', ch: '房地产代理/代理' },
            //{ t: 'a', en: 'Home private seller', ch: '家庭私人卖家' },
            { t: 'b', en: 'Home buyer', ch: '购房者' },
            //{ t: 'a', en: 'Settlement agent', ch: '结算代理人' },
            //{ t: 'a', en: 'Home loan lender', ch: '住房贷款机构' },
            //{ t: 'a', en: 'Home loan broker', ch: '房屋贷款经纪人' },
            //{ t: 'a', en: 'Property manager for lease', ch: '物业经理出租' },
            //{ t: 'a', en: 'Landlord for lease', ch: '房东出租' }
        ],
        googleMapsApiKey: 'AIzaSyA7IA-SEV3Ss2dOLncb5XZ6iAA1YQcjFy4',
    };

    $scope.translate = function (list, key, isEnglish) {
        // translate from en to ch and vice versa
        for (var i = 0; i < list.length; i++) {
            if (isEnglish) {
                if (list[i].en == key) {
                    return list[i].ch;
                }
            }
            else {
                if (list[i].ch == key) {
                    return list[i].en;
                }
            }
        }
        return key;
    };
    $scope.translateByKey = function (list, key, isEnglish) {
        // use k to fetch either en or ch
        for (var i = 0; i < list.length; i++) {
            if (list[i].k == key) {
                if (isEnglish) return list[i].en;
                return list[i].ch;
            }
        }
        return k;
    };

    // search results stored here
    $scope.queryParams = {};
    $scope.listings = [];
    $scope.userlistings = [];
    $scope.wishlist = [];
    $scope.newlist = {              // the new listing form object
        address: {},
        fileread: []
    };
    $scope.orderParams = {
        orderBy: $scope.static.sortTypes[0].en
    };

    // UI
    $scope.ui = {
        isLanding: true,                    // true if the window is currently on landing page
        landingSearchTab: 'Buy',            // The selected search tab on the landing page
        isMobileDevice: mobileCheck(),      // true if the browser device is a mobile 
        en: getCookie('lang') == 'en',      // true if english, false if chinese
        currSelectedPanelId: -1,            // the actual listing id that is currently selected
        currDetailedPanelId: -1,            // the id of the hidden detail panel
        currSelectedImageIndex: -1,         // $index, not id
        galleryIndex: 0,                    // the index of the gallery image
        panels: [],

        size: {
            height: 0,
            width: 0,
            headerHeight: 60,               // default size of the header bar
            headerNavLeft: 275,             // correct size is between 250 and 300 so we take the middle
            contentBarLeft: 275,            // unless the searchbar is hidden, this will be = headerNavLeft
            searchBarLeft: 0
        },

        mobileMaxWidth: 640,

        currMainDisplay: 'listings',

        login: {
            customMsg: null,
            result: {                   // ui display of the login result
                msg: '',
                isIncorrectPassword: ''
            },
            signupResult: {
                msg: ''
            }
        },

        // these events tell the preloader to disappear
        progressBar: 0,
        userDataLoaded: true,
        listingDataLoaded: false,

        notifs: [],                      // the user notifications

        chatBox: {
            listing: null,            // if chatting, will be set to a listing object
        },
        inbox: {
            currSelectedUser: 0
        },

        // indicates if elements shown - all false by default to avoid flickering on pageload
        shown: {
            bottomNav: false,
            mobileSearch: false,
            searchBar: true,
            searchByLocation: true,
            searchByPrice: true,
        },

        // Methods here
        updateProgressBar: function (newVal) {

            var oldVal = $scope.ui.progressBar;
            var j = 0;
            for (var i = oldVal; i <= newVal + 0.1; i += 1) {

                var state = i;
                $timeout(function (state) { $scope.ui.progressBar = state; }, j * 10, true, state);
                j++;
            }

            // The final step - actually set it to the newVal - important if (newVal - oldVal) is not a multiple of 1
            $timeout(function (state) { $scope.ui.progressBar = state; }, j * 10, true, newVal);
        },
        addNotif: function (msg) {
            console.log('added notification');

            var len = $scope.ui.notifs.length;
            $scope.ui.notifs.push({
                id: len,
                en: msg.en,
                ch: msg.ch,
                icon: msg.icon
            });

            // message disappears after 3000ms
            $timeout(function () {
                var index = findIndexById($scope.ui.notifs, len);
                $scope.ui.notifs.splice(index, 1);
            }, 3000);
        },
        onInactivityLogout: function () {
            // This is only used by 2nd layer defences to catch when user is not logged out for some reason,
            // and is trying to access a private resource
            $scope.onSuccessfulLogout();

            // get user to sign in again
            $scope.ui.loginView = 'login';
            $scope.ui.login.customMsg = {
                en: 'Hi there! Looks like you\'ve gone away for a while, so we logged you out to protect your privacy. Please log in again.',
                ch: '您好！看起来你已经离开了一段时间，所以我们让你出去保护你的隐私。请再次登录。'
            }
            $scope.ui.topElement = 'login';
        },
        isMobileDisplay: function () {
            // check if browser is registered mobile device
            if ($scope.ui.isMobileDevice) return true;

            // otherwise, base on screen width alone
            if ($scope.ui.size == null) return false;
            return $scope.ui.size.width <= $scope.ui.mobileMaxWidth;
        },
        confirmAction: function (msg, label, confirmedCallback, icon) {
            $scope.ui.confirmActionMsg = msg;
            $scope.ui.confirmLabel = label;
            $scope.ui.confirmActionCallback = confirmedCallback;
            $scope.ui.confirmActionIcon = icon == undefined ? 'em' : icon;
            $scope.ui.showConfirmActionForm = true;
        },
        displayListings: function (resp, clear) {
            
            // This response has a specific structure - these are the requirements
            $scope.queryParams = resp.query;

            if (clear) {
                $scope.listings.length = 0;
            }

            if (resp.listings != undefined) {
                // Update the scope variable - if already present, don't add it!
                for (var i = 0; i < resp.listings.length; i++) {
                    if (findIndexById($scope.listings, resp.listings[i].id) < 0) {
                        $scope.listings.push(resp.listings[i]);
                    }
                }
            }
            if (resp.extralistings != undefined) {
                // Add the extra listings on - in future move this to the server side...
                for (var i = 0; i < resp.extralistings.length; i++) {
                    if (findIndexById($scope.listings, resp.extralistings[i].id) < 0) {
                        $scope.listings.push(resp.extralistings[i]);
                    }
                }
            }
            
            // Begin format
            recalculateScopeParameters($scope);
            $scope.ui.panels = chunk($scope.listings, $scope.ui.nCol);
            repaintBody($scope);

            // reset 
            $scope.ui.currSelectedPanelId = -1;
            $scope.ui.currDetailedPanelId = -1;
            $scope.ui.listingDataLoaded = true;
        }
    };
    $scope.methods = {
        addToWishlist: function (listing) {

            if ($scope.acc == null) {
                // login required
                $scope.ui.loginView = 'login';
                $scope.ui.login.customMsg = {
                    en: 'Please log in to continue.',
                    ch: '请登录访问更多内容。'
                }
                $scope.ui.topElement = 'login';
                return;
            }
            if ($scope.acc.wishlist == null) {
                console.log('Empty wishlist');
                return;
            }

            // show a loading thing here

            $http.post('/wishlist',
                {
                    aid: $scope.acc.id,
                    lid: listing.id,
                    a: 'a'
                }).then(
                    function success(res) {
                        $scope.acc.wishlist.push(res.data.id);
                        $scope.ui.addNotif({
                            en: 'Added to your wishlist!',
                            ch: '添加到您的收藏夹!',
                            icon: 'tick'
                        });
                    },
                    function error(err) {

                        if (err.status == 401) {
                            $scope.ui.onInactivityLogout();
                            return;
                        }
                    }
                )
        },
        removeFromWishlist: function (listing, callback) {
            if ($scope.acc == null) {
                $scope.ui.loginView = 'login';
                $scope.ui.login.customMsg = {
                    en: 'Please log in to continue.',
                    ch: '请登录访问更多内容。'
                }
                $scope.ui.topElement = 'login';
                return;
            }

            if ($scope.acc.wishlist == null) {
                console.log('Empty wishlist');
                return;
            }

            // TODO: show a loading thing here

            $http.post('/wishlist',
                {
                    aid: $scope.acc.id,
                    lid: listing.id,
                    a: 'r'
                }).then(
                    function success(res) {

                        // Remove all instances - hence why we dont use findIndexById
                        var wl = $scope.acc.wishlist;
                        for (var i = wl.length - 1; i >= 0; i--) {
                            if (wl[i] == listing.id) {
                                wl.splice(i, 1);
                            }
                        }
                        $scope.ui.addNotif({
                            en: 'Removed from your wishlist.',
                            ch: '从您的收藏中删除',
                            icon: 'tick'
                        });

                        if (callback != undefined) {
                            callback(res);
                        }
                    },
                    function error(err) {

                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }

                        $scope.ui.addNotif({
                            en: 'Unable to remove item from your wishlist at this time. Please try again later.',
                            ch: '目前无法从您的心愿单中删除项目。请稍后再试。',
                            icon: 'em'
                        });

                        if (callback != undefined) {
                            callback(err);
                        }
                    }
                )
        },
        getGoogleMapSrc: function (address) {
            // Given street address, suburb, state, postcode
            var query = address.street.trim() + ', ' + address.suburb.trim() + ' ' + address.state.trim() + ' ' + address.postcode;
            query = query.split(' ').join('+');
            return "https://www.google.com/maps/embed/v1/place?key=" + $scope.static.googleMapsApiKey + "&q=" + query;
        },
        // dynamic offsite urls
        trustAsResourceUrl: $sce.trustAsResourceUrl
    };

    // populate listing form for editting an existing listing
    $scope.ui.populateListingForm = function (listing, mode) {

        $scope.clearListingForm();

        if (listing == undefined) return;

        $scope.newlist.userMode = mode;
        $scope.newlist.ltype = $scope.getListingTypeByKey(listing.ltype);
        $scope.newlist.type = listing.type;
        $scope.newlist.id = listing.id;

        // Only show suburb suggestions if there is at least 1 missing field 
        if (listing.address.suburb == null || listing.address.state == null || listing.address.postcode == null) {
            $scope.newlist.address.showSuggestions = true;
        }
        $scope.newlist.address = listing.address;

        // We assume that if there is a map link provided, it is good
        if (listing.address.mapSrc != null && listing.address.mapSrc.length > 0) {
            $scope.newlist.address.mapSaved = true;
        }

        $scope.newlist.price = listing.trueprice;
        if (listing.price != undefined) {

            $scope.newlist.pricedescriptiontype = listing.price.type.replace('_', ' ');
            switch (listing.price.type) {
                case 'Fixed_price':
                case 'From':
                case 'Over':
                case 'Offers_from':
                case 'Low':
                case 'Mid':
                case 'High':
                    $scope.newlist.pricedescription = listing.price.price;
                    break;
                case 'Between':
                    $scope.newlist.pricedescription1 = listing.price.price1;
                    $scope.newlist.pricedescription2 = listing.price.price2;
                    break;
            }
        }

        $scope.newlist.features = {};
        for (var i = 0; i < listing.features.length; i++) {
            // no search value - boolean
            if (listing.features[i].v == undefined) {
                $scope.newlist.features[listing.features[i].type] = true;
            }
            else {
                $scope.newlist.features[listing.features[i].type] = listing.features[i].v;
            }
        }
        // required features
        if ($scope.newlist.features['Bedrooms'] == undefined) $scope.newlist.features['Bedrooms'] = 0;
        if ($scope.newlist.features['Bathrooms'] == undefined) $scope.newlist.features['Bathrooms'] = 0;
        if ($scope.newlist.features['Garages'] == undefined) $scope.newlist.features['Garages'] = 0;

        // inspection times
        $scope.newlist.inspectiontimes = [];
        for (var i = 0; i < listing.inspections.length; i++) {
            var ins = listing.inspections[i];
            $scope.newlist.inspectiontimes.push({
                display: formatDateTimeInterval(ins.start, ins.end, 'en-AU', true),
                start: formatDateTime(ins.start, true),
                end: formatDateTime(ins.end, true)
            });
        }
        $scope.newlist.title = listing.title;
        $scope.newlist.description = listing.description;

        $scope.newlist.fileread.length = 0;
        //console.log('populating with: ' + JSON.stringify(listing.imageIds));
        for (var i = 0; i < listing.imageIds.length; i++) {
            var url = '/images/' + listing.imageIds[i].i;
            $scope.newlist.fileread.push({
                filename: 'Image ' + (i + 1),
                origin: 's',
                id: listing.imageIds[i],
                isReplaced: false,
                result: url
            });
        };
        //console.log('fileread is now: ' + JSON.stringify($scope.newlist.fileread));

        return $scope.newlist;
    };
    // reset the post listing page with 'new'
    $scope.ui.populateNewListingForm = function () {
        $scope.clearListingForm();
        $scope.newlist.userMode = 'new';
    };
    $scope.clearListingForm = function () {
        $scope.newlist = {
            address: {},
            fileread: []
        };
    };

    $scope.getListingTypeByKey = function (k) {
        for (var i = 0; i < $scope.static.listingTypes.length; i++) {
            if ($scope.static.listingTypes[i].k == k) {
                return $scope.static.listingTypes[i];
            }
        }
        return null;
    }

    // These methods are called from the header...
    $scope.getWishlist = function () {
        $http.post('/wishlist/fetch', {
                aid: $scope.acc.id
            }
        ).then(
            function success(res) {
                console.log(res);
                $scope.wishlist = res.data.wishlist;
            },
            function error(err) {
                console.log(err);
            }
        );
    }
    $scope.getUserListings = function () {

        $http.post('/userlistings', {
            aid: $scope.acc.id,
            since: 0
        }).then(
            function success(res) {
                console.log(res.data.listings);
                $scope.userlistings = res.data.listings;
            },
            function error(err) {
                if (err.status == 401) {
                    $scope.ui.onInactivityLogout();
                    return;
                }
            }
        )
    };
    // When user clicks on the down arrow in mobile displays
    $scope.showMobileSearchPanel = function () {

        $scope.ui.shown.mobileSearch = true;

        var searchBar = document.getElementById('searchBar');
        searchBar.style.bottom = 0;
        searchBar.style.height = 'auto';
    }
    $scope.hideMobileSearchPanel = function () {
        $scope.ui.shown.mobileSearch = false;
        var searchBar = document.getElementById('searchBar');
        searchBar.style.height = 0;
    }

    // Fake load
    $scope.ui.updateProgressBar(50);

    // Get sprites 
    $scope.getSprites = function () {
        console.log('getting sprites');
        $http.get('/sprites', {}).then(
            function success(res) {
                console.log(res);
                $scope.ui.sprites = res.data;
            },
            function error(err) {
                console.log(err);
            }
        );
    }
    $scope.getSprites();
    $scope.ui.updateProgressBar(80);

    // watch window resize
    angular.element($window).on('resize', function () {

        var prevWidth = $scope.ui.size.width;
        recalculateScopeParameters($scope);

        // resize on width change only 
        if (prevWidth != $scope.ui.size.width) {
            if (!$scope.ui.isLanding) {
                repaintBody($scope);
            }
            $scope.$apply();
        }
    });
    
    $scope.ui.resizeDetailedPanel = function (headerHeight, searchBarWidth) {

        // The onus will be on us to make sure that its not shwon in mobile display
        var top = (headerHeight == undefined ? $scope.ui.size.headerHeight : headerHeight);
        var sBarWidth = searchBarWidth;
        if (sBarWidth == undefined) {
            sBarWidth = $scope.ui.shown.searchBar ? clamp($scope.ui.size.width / 5, 250, 300) : 0;
        }

        var cb = document.getElementById('contentBar')
        if (cb == undefined) return;

        var left = sBarWidth + Math.round(100 / $scope.ui.nCol) / 100 * cb.clientWidth;

        console.log('recalculating detailed panel dimensions');
        $scope.ui.detailedPanel2 = {
            top: top + 'px',
            left: left + 'px',
            height: ($scope.ui.size.height - top) + 'px',
            width: ($scope.ui.size.width - left) + 'px'
        }
    }

    $scope.$watch('ui.size.searchBarLeft', function (newVal, oldVal) {
        console.log('search bar left changed to: ' + newVal);
    });

    // initialise the page ui once the page is loaded
    angular.element(document).ready(function () {
        // I don't think this event is ever needed here because we are on the landing page.
        initPage($scope);
        $scope.ui.updateProgressBar(100);
    });
});

app.directive("fileread", ['$http', function ($http) {
    return {
        scope: {
            fileread: "=",
        },
        link: function (scope, element, attrs) {
            element.bind("change",
                function (changeEvent) {
                    for (var i = 0; i < changeEvent.target.files.length; i++) {
                        var file = changeEvent.target.files[i];

                        var reader = new FileReader();
                        reader.onload = (function (f) {

                            return function (loadEvent) {
                                scope.$apply(
                                    function () {
                                        scope.fileread.push({
                                            filename: f.name,
                                            origin: 'c',
                                            result: loadEvent.target.result,
                                        });
                                    }
                                );
                            };

                        })(file);
                        reader.readAsDataURL(file);
                    }

                    element.val(null);
                });

        }
    }
}]);

function dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    var array = [];
    for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
    }
    return new Blob([new Uint8Array(array)], {
        type: mimeString
    });
}
function getWindowSize() {
    var body = document.body, html = document.documentElement;
    return {
        height: window.innerHeight || html.clientHeight || body.clientHeight,
        width: window.innerWidth || html.clientWidth || body.clientWidth
    }
}
function clamp(x, min, max) {
    if (x > max) x = max;
    if (x < min) x = min;
    return x;
}
function repaintBody($scope) {

    var size = $scope.ui.size;
    var contentBar = document.getElementById("contentBar");

    if (!$scope.ui.isMobileDisplay()) {
        if (contentBar != null) {
            contentBar.style.height = (size.height - size.headerHeight) + 'px';
        }
        $scope.ui.resizeDetailedPanel(size.headerHeight, size.headerNavLeft);
    }
    else {

        // mobile display.. this is where things get tricky
        // We need to re-calculate a few variables each time 

        if (contentBar != null) {
            var marginTop = size.headerHeight;
            contentBar.style.height = (size.height - marginTop) + 'px';
        }
    }
}
function calculateNCol($scope) {

    // If its a mobile display, use a harsher requirement
    if ($scope.ui.isMobileDisplay()) {
        //if ($scope.ui.size.width > 1500) return 2;
        return 1;
    }

    // Otherwise, use normal screen requirements
    if ($scope.ui.size.width > 2000) return 4;
    if ($scope.ui.size.width > 1350) return 3;
    if ($scope.ui.size.width > 950) return 2;
    return 1;
}
function recalculateScopeParameters($scope) {

    $scope.ui.size = getWindowSize();

    if ($scope.ui.size == null || !$scope.ui.isMobileDisplay()) {
        $scope.ui.size.headerHeight = 60;   // default
        $scope.ui.size.headerNavLeft = clamp($scope.ui.size.width / 5, 250, 300);

        if ($scope.ui.shown.searchBar) {
            $scope.ui.size.contentBarLeft = $scope.ui.size.headerNavLeft;
        }
        else {
            $scope.ui.size.contentBarLeft = 0;
            $scope.ui.size.searchBarLeft = -$scope.ui.size.headerNavLeft;
        }
    }
    else {
        $scope.ui.size.headerHeight = 0.1 * $scope.ui.size.width;
        $scope.ui.size.headerNavLeft = 0;
        $scope.ui.size.contentBarLeft = 0;
    }

    var nCol = calculateNCol($scope);
    if (nCol != $scope.ui.nCol) {
        $scope.ui.nCol = nCol;
        // rearrange panels
        if ($scope.ui.nCol > 0 && $scope.listings) {
            $scope.ui.panels = chunk($scope.listings, $scope.ui.nCol);
        }
    }

    // calculate the max number of features per listing, for the given page width
    $scope.ui.maxFeatures = calcMaxFeaturesPerListing($scope);
}
function calcMaxFeaturesPerListing($scope) {
    if ($scope.ui.nCol <= 1) return 5;

    var w = $scope.ui.size.width;

    if ($scope.ui.nCol == 2) {
        if (w < 1000) return 3;
        if (w < 1120) return 4;
        return 5;
    }
    else {
        if (w < 1370) return 3;
        if (w < 1585) return 4;
        return 5;
    }
}
function initPage(scope) {
    recalculateScopeParameters(scope);
    repaintBody(scope);
}