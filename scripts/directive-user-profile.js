app.directive('gkUserProfile', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'userProfile.html',
        scope: {
            acc: '=',
            ui: '=',
            methods: '=',
        },
        link: function(scope) {
            scope.updatePassword = function(newpass) {

                if (scope.acc == null) {
                    scope.updatePasswordResult = {
                        en: 'Please log in to change your password.',
                        ch: '请登录以更改密码。'
                    }
                    return;
                }
                if (newpass == null) {
                    scope.updatePasswordResult = {
                        en: 'Please enter your password.',
                        ch: '请输入您的密码。'
                    }
                    return;
                }
                if (newpass.oldPassword == null) {
                    scope.updatePasswordResult = {
                        en: 'Please enter your old password.',
                        ch: '请输入您的旧密码。' 
                    }
                    return;
                }
                if (newpass.newPassword1 == null) {
                    scope.updatePasswordResult = {
                        en: 'Please enter your new password.',
                        ch: '请输入您的新密码。'
                    }
                    return;
                }
                if (newpass.newPassword2 == null) {
                    scope.updatePasswordResult = {
                        en: 'Please enter your new password again.',
                        ch: '请再次输入您的新密码。'
                    }
                    return;
                }
                if (newpass.newPassword1 != newpass.newPassword2) {
                    scope.updatePasswordResult = {
                        en: 'Passwords do not match.',
                        ch: '密码不匹配。'
                    }
                    return;
                }

                $http.post('/account', { action: 'updatepassword', aid: scope.acc.id, oldp: newpass.oldPassword, p: newpass.newPassword1 })
                .then(
                    function success(res) {

                        scope.acc.passwordlastupdated = res.data.lastupdated;
                        scope.updatingPassword = false;
                        scope.ui.addNotif({
                            en: 'Password successfully updated.',
                            ch: '密码已成功更新。',
                            icon: 'tick'
                        });
                    },
                    function error(err) {
                        
                        if (err.status == 401) {
                            scope.updatePasswordResult = {
                                en: 'Incorrect password. Please try again.',
                                ch: '密码错误。请再试一次。'
                            };
                        }
                        else {
                            scope.updatePasswordResult = {
                                en: 'We are unable to change your password at this time. Please try again later.',
                                ch: '目前无法更改您的密码。请稍后再试。'
                            };
                        }
                        
                    }
                )
            }
            scope.updateEmailAddress = function(newemail, password) {

                if (newemail == undefined || !isEmail(newemail)) {
                    scope.newemailresult = {
                        en: 'Please enter a valid email address.',
                        ch: '请输入您的新电子邮件地址。'
                    };
                    return;
                }
                if (password == undefined || password.length == 0) {
                    scope.newemailresult = {
                        en: 'Please enter your account password.',
                        ch: '请输入您的帐户密码。'
                    };
                    return;
                }

                $http.post('/account', { action: 'updateemail', aid: scope.acc.id, e: newemail, p: password}).then(
                    function success(res) {

                        scope.changingEmail = false;
                        scope.acc.email = res.data.email;
                        scope.ui.addNotif({
                            en: 'Successfully changed your email address.',
                            ch: '已成功更改您的电子邮件地址。',
                            icon: 'tick'
                        });
                    },
                    function error(err) {
                        if (err.status == 401) {
                            scope.newemailresult = {
                                en: 'Incorrect password. Please try again.',
                                ch: '密码错误。请再试一次。'
                            };
                        }
                        else {
                            scope.newemailresult = {
                                en: 'We are unable to update your email address at this time. Please try again later.',
                                ch: '我们目前无法更新您的电子邮件地址。请稍后再试。'
                            };
                        }
                    }
                );
            }
            scope.formatDate = function (time) {
                return (new Date(time)).toLocaleString();
            }
        }
    }
});

app.directive('gkBannerLoaded', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                console.log(element);
                scope.$apply();
            });
            element.bind('error', function(){
                console.log('error - TODO message');
                scope.$apply();
            });
        }
    };
})