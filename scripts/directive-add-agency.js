app.directive('gkAddAgency', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'addAgency.html',
        scope: {
            ui: '=',
            acc: '=',
            addingAgency: '=',
            selectable: '=',
            selected: '='
        },
        link: function (scope, element, attrs) {

            scope.setSelectedAgency = function (ag) {
                if (scope.selectable) scope.selected = ag;
            }

            scope.addAgency = function () {

                if (scope.acc == null) {
                    agency.result = {
                        en: 'Please log in to continue.',
                        ch: '请登录访问更多内容。'
                    }
                    return;
                }

                $http.post('/addagency', { aid: scope.acc.id }).then(
                    function success(res) {

                        var ag = res.data.agency;
                        ag.isEditting = true;
                        scope.acc.agencies.unshift(ag);

                        // Notification
                        scope.ui.addNotif({
                            en: 'Agency profile successfully added.',
                            ch: '代理商档案成功添加。',
                            icon: 'tick'
                        })
                    },
                    function error(err) {

                        console.log(err);
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }

                        scope.ui.addNotif({
                            en: 'Cannot add agency profile at this time. Please try again later.',
                            ch: '目前无法添加代理商资料。请稍后再试。',
                            icon: 'em'
                        })
                    }
                )
            }
            scope.updateAgency = function (ag) {

                // Key editables only 
                if (scope.acc == null) {
                    agency.result = {
                        en: 'Please log in to continue.',
                        ch: '请登录访问更多内容。'
                    }
                    return;
                }

                // Set loading display
                if (scope.agencyUpdatePending) {
                    return;
                }
                scope.agencyUpdatePending = true;

                // Upload images first .. if any
                if (ag.replaceLogos.length > 0) {

                    var image = ag.replaceLogos[ag.replaceLogos.length - 1].result;
                    if (image == null || scope.acc == null) {
                        return;
                    }

                    var fd = new FormData();
                    fd.append('file', dataURItoBlob(image));
                    fd.append('aid', scope.acc.id);
                    fd.append('agencyId', ag.id);
                    fd.append('filetype', 'agencyImg');

                    $http.post('/files', fd, {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined },
                        timeout: 30000
                    }).then(
                        function success () {
                            scope.postUpdateAgency(ag);
                        },
                        function error (err) {
                            ag.isEditting = false;
                            if (err.status == 401) {
                                scope.ui.onInactivityLogout();
                                return;
                            }
                            else if (err.status == 413) {
                                scope.ui.addNotif({
                                    en: 'The image is too big. Please try again with a smaller one!',
                                    ch: '您上传的图片太大了。请再试一次小一个！',
                                    icon: 'em'
                                });
                            }
                            scope.agencyUpdatePending = false;
                        }
                    );
                }
                else {
                    scope.postUpdateAgency(ag);
                }
            }
            scope.postUpdateAgency = function (ag) {

                // Requires this additional method because of the conditional post new image 
                $http.post('/updateagency', { 
                    aid: scope.acc.id, 
                    agency: {
                        id: ag.id,
                        lid: ag.lid,
                        hex: ag.hex,
                        name: ag.name,
                        address: ag.address,
                        website: ag.website,
                    }
                }).then(
                    function success(res) {

                        // Find and replace
                        var index = findIndexById(scope.acc.agencies, ag.id);
                        if (index >= 0) {
                            scope.acc.agencies[index] = res.data.agency;
                        }

                        // Add notification
                        scope.ui.addNotif({
                            en: 'Your agency profile has been updated.',
                            ch: '您的代理商资料已更新。',
                            icon: 'tick'
                        });
                        scope.agencyUpdatePending = false;
                    },
                    function error(err) {
                        ag.isEditting = false;
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }

                        scope.ui.addNotif({
                            en: 'Cannot update your agency profile at this time. Please try again later.',
                            ch: '目前无法更新您的代理商资料。请稍后再试。',
                            icon: 'em'
                        });
                        scope.agencyUpdatePending = false;
                    }
                )
            }
            scope.deleteAgency = function (ag) {

                var confirmMsg = {
                    en: 'Are you sure you wish to delete this agency profile?',
                    ch: '您确定要删除此代理机构资料吗？'
                };
                if (ag != undefined && ag.name != undefined && ag.name.length > 0) {
                    confirmMsg.en = 'Are you sure you wish to delete the agency profile "' + ag.name + '"?';
                }

                scope.ui.confirmAction(confirmMsg, { en: 'Delete', ch: '删除' },
                    function () {
                        $http.post('/deleteagency', { aid: scope.acc.id, agencyId: ag.id }).then(
                            function success(res) {

                                var index = findIndexById(scope.acc.agencies, ag.id);
                                if (index >= 0) {
                                    scope.acc.agencies.splice(index, 1);
                                    scope.ui.addNotif({
                                        en: 'Successfully deleted agency profile',
                                        ch: '已成功删除代理商资料',
                                        icon: 'tick'
                                    });
                                }
                            },
                            function error(err) {
                                console.log(err);
                                if (err.status == 401) {
                                    scope.ui.onInactivityLogout();
                                    return;
                                }

                                scope.ui.addNotif({
                                    en: 'Unable to delete this agency profile at this time. Please try again later.',
                                    ch: '目前无法删除此代理商资料。请稍后再试。',
                                    icon: 'em'
                                })
                            }
                        )
                    }
                )
            }

            var bannerCtx;
            scope.currSelectedAgency;
            scope.loadBannerColorPicker = function (agency) {

                scope.currSelectedAgency = agency;
                scope.colourPickerLoaded = false;

                var canvasEl = document.getElementById('bannerColorPicker');
                bannerCtx = canvasEl.getContext('2d');

                var image = new Image(500, 200);
                image.onload = function () {
                    bannerCtx.drawImage(image, 0, 0, image.width, image.height);
                    scope.colourPickerLoaded = true;
                    scope.$apply();
                }
                image.src = "./resources/color_picker.png";
            }

            scope.currSampledColor = '#ffffff';
            scope.changeColor = function (event) {
                if (!scope.trackingColor || bannerCtx == undefined) {
                    return;
                }
                scope.repositionSelectorRect(event.offsetX, event.offsetY);

                var imgData = bannerCtx.getImageData(event.offsetX, event.offsetY, 1, 1);
                var d = imgData.data;
                scope.currSampledColor = rgbToHex(d[0], d[1], d[2]);
                console.log(event.offsetX, event.offsetY, scope.currSampledColor);
            }
            scope.repositionSelectorRect = function (x, y) {
                if (bannerCtx == undefined) return;

                var canvas = document.getElementById('bannerColorPicker');
                var rect = document.getElementById('colorPickerRect');

                var rectWidth = canvas.width / 37, rectHeight = canvas.height / 15;
                rect.style.left = (Math.floor(x / rectWidth) * rectWidth - 1) + 'px';
                rect.style.top = (Math.floor(y / rectHeight) * rectHeight - 1) + 'px';
            }
            scope.startTrackingColor = function() {
                console.log('setting scope.trackingColor = true');
                scope.trackingColor = true;
            }
            scope.stopTrackingColor = function () {
                console.log('setting scope.trackingColor = false');
                scope.trackingColor = false;
            }
            scope.saveColor = function () {
                scope.currSelectedAgency.hex = scope.currSampledColor;
                scope.colourPickerLoaded = false;// hide the color picker
            }
        }
    }
});
function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";

    var rs = ("0" + r.toString(16)).slice(-2);
    var gs = ("0" + g.toString(16)).slice(-2);
    var bs = ("0" + b.toString(16)).slice(-2);
    return '#' + rs + gs + bs;
}
function getSiblingOfClass(element, className) {
    var siblings = element.parentNode.childNodes;
    for (var i = 0; i < siblings.length; i++) {
        if (siblings[i].className != undefined && siblings[i].className.indexOf(className) >= 0) {
            return siblings[i];
        }
    }
    return null;
}