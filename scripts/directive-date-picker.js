app.directive('gkDatePicker', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'datePicker.html',
        scope: {
            ui: '=',
            width: '=',
            selectedDate: '='
        },
        link: function(scope, element, attrs) {
            
            scope.daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
            const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

            // Initialise to today
            var date = new Date();
            scope.monthIndex = date.getMonth();
            scope.year = date.getFullYear();
            scope.dayIndex = date.getDate();
            scope.selectedDate = date;

            scope.incrementMonth = function (increment) {

                var monthIndex = scope.monthIndex + increment;
                if (monthIndex < 0) {
                    monthIndex += 12;
                    scope.year--;
                }
                else if (monthIndex >= 12) {
                    monthIndex -= 12;
                    scope.year++;
                }
                scope.setMonth(monthIndex, scope.year);
            }
            scope.setMonth = function (monthIndex, year) {
                scope.monthIndex = monthIndex;
                scope.month = monthNames[monthIndex] + ' ' + year;

                const daysInMonth = [ 31, isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                scope.days = getDateMatrix(new Date(year + '-' + (monthIndex + 1) + '-1'), daysInMonth);
            }
            
            scope.selectDate = function (date) {

                if (!date.isCurrMonth) {
                    if (date.date < 15) {
                        scope.incrementMonth(1);
                    }
                    else {
                        scope.incrementMonth(-1);
                    }
                }
                scope.selectedDate = new Date(scope.year + '-' + (scope.monthIndex + 1) + '-' + date.date);
            }
            // Check if the provided day = selectedDate
            scope.isSelectedDate = function (d) {
                if (!d.isCurrMonth) return false;
                if (scope.monthIndex != scope.selectedDate.getMonth()) return false;
                if (scope.year != scope.selectedDate.getFullYear()) return false;
                return d.date == scope.selectedDate.getDate();
            }

            scope.setMonth(scope.monthIndex, scope.year);
        }
    }
});
function isLeapYear(year)
{
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}
function getDateMatrix(today, daysInMonth) {
    
    var calendar = [];

    var date = today.getDate();
    var dayOfWeek = today.getDay();
    var month = today.getMonth();

    var prevMonthDays = daysInMonth[(month + 11) % 12];
    var currMonthDays = daysInMonth[(month + 12) % 12];

    var dayOfWeek_1stDayOfMonth = (dayOfWeek - ((date - 1) % 7) + 7) % 7;

    var week = [];
    var i;
    for (i = 1; i <= currMonthDays; i++) {
        var day = (dayOfWeek_1stDayOfMonth + (i - 1)) % 7;
        if (day == 0 && week.length > 0) {
            calendar.push(week);
            week = [];
        }
        week.push({ 
            date: i, 
            isCurrMonth: true
        });
    }
    if (week.length > 0) {
        calendar.push(week);
    }

    // Complete first week
    i = 0;
    while (calendar[0].length < 7) {
        calendar[0].unshift({
            date: prevMonthDays - i, 
            isCurrMonth: false 
        });
        i++;
    }

    // Complete last week
    i = 1;
    var lastWeek = calendar[calendar.length - 1];
    while (lastWeek.length < 7) {
        lastWeek.push({
            date: i,
            isCurrMonth: false
        });
        i++;
    }

    return calendar;
}
function copy(array) {
    var copy = [];
    for (var i = 0; i < array.length; i++) {
        copy.push(array[i]);
    }
    return copy;
}