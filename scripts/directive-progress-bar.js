app.directive('gkProgressBar', function($timeout, $interval) {
    return {
        restrict: 'E',
        templateUrl: 'progressBar.html',
        scope: {
            state: '=',  // from 0 to 100
            description: '='
        },
        link: function(scope, element, attrs) {

            scope.$watch('state', function(newVal, oldVal) {
                if (newVal != oldVal && newVal != null) {

                    if (newVal == 0) {
                        // 0 is special as it is a reset - so we don't do a transition
                        scope.state = 0;
                    }
                    else {
                        scope.transition(oldVal, newVal);
                    }
                }
            });

            scope.transition = function (oldVal, newVal) {

                var j = 0;
                for (var i = oldVal; i <= newVal + 0.1; i += 1) {

                    var state = i;
                    $timeout(function(state) {
                        scope.smoothState = state;
                    }, 
                    j * 10, true, state);
                    j++;
                }

                // The final step - actually set it to the newVal - important if (newVal - oldVal) is not a multiple of 1
                $timeout(function(state) {
                    scope.smoothState = state;
                }, 
                j * 10, true, newVal);
            }

            // smooth state is just for the ui
            scope.transition(0, scope.state);
        }
    }
});