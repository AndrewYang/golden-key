app.directive('gkInbox', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'inbox.html',
        scope: {
            acc: '=',
            ui: '=',
        },
        link: function(scope, element, attrs) {

            // this definition is different to the 'isMobileDisplay' - because we need more room here
            scope.isSmallDisplay = function () {
                return scope.ui.isMobileDevice || scope.ui.size.width <= 1080;
            }
            scope.isSentByMe = function(conv, msg) {
                return conv.otheruser.id != msg.fromUid;
            }
            scope.getCounterpartyName = function (conv) {
                if (conv == undefined || conv.otheruser == undefined) return undefined;

                var u = conv.otheruser;
                if (u.fname == undefined || u.fname.length == 0) return 'Anonymous';
                return u.fname + ' ' + u.lname;
            }
            scope.getDateString = function (time) {
                var options = {
                    day: '2-digit',
                    month: '2-digit',
                    hour: 'numeric',
                    minute: '2-digit'
                }
                return new Date(time).toLocaleString('en-AU', options);
            }
            scope.getTimeString = function (time) {
                var options = {
                    hour: 'numeric',
                    minute: '2-digit'
                }
                return new Date(time).toLocaleTimeString('en-AU', options);
            }
            scope.truncateString = function (message, len) {
                if (message.length > len) {
                    return message.substring(0, len) + ' ...';
                }
                return message;
            }
            scope.isSeenByMe = function (conv) {
                
                // Rule - check the last message only

                if (!(conv.messages.length > 0)) {
                    return false;
                }

                var last = conv.messages[conv.messages.length - 1];

                // If we sent the message, then seen is always true
                if (last.fromUid != conv.otheruser.id) {
                    return true;
                }
                return last.seen;
            }
            scope.getUserInbox = function () {
                if (scope.acc == null) return;

                $http.post('/inbox', { 
                    aid: scope.acc.id
                }).then(
                    function success(res) {
                        scope.acc.convs = res.data.convs;

                        // Fetch the first conversation
                        if (scope.acc.convs.length > 0) {
                            scope.getConversation(scope.acc.convs[0]);
                        }
                    },
                    function error(err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                        }
                    }
                )
            }
            scope.getConversation = function (conv) {

                scope.selectedConv = conv;
                console.log('setting selected conv to ' + scope.selectedConv.id)

                $http.post('/conv/messages', { 
                    id: conv.id, 
                    aid: scope.acc.id
                }).then(
                    function success(res) {
                        conv.messages = res.data.messages;

                        // scroll to bottom 
                        var element = document.getElementById("inbox-msg-tbl");
                        element.scrollTop = element.scrollHeight;
                    },
                    function error(err) {
                        console.log(err);
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                    }
                );
                scope.setAsSeen(conv);
            }
            scope.setAsSeen = function (conv) {

                if (scope.isSeenByMe(conv)) {
                    return;
                }
                if (scope.acc == null) {
                    scope.ui.onInactivityLogout();
                    return;
                }

                $http.post('/readconv', {
                    convId: conv.id,
                    aid: scope.acc.id
                }).then(
                    function success (res) {
                        conv.messages[conv.messages.length - 1].seen = true;
                    },
                    function error (err) {
                        console.log(err);
                    }
                )
            }
            scope.send = function(conv, newText) {

                if (scope.sendMessagePending) {
                    return;
                }
                if (conv == undefined) return;
                if (newText == undefined || newText.length <= 0) return;
                
                scope.sendMessagePending = true;

                $http.post('/messages', { 
                    text: newText.msg,
                    accountId: scope.acc.id,
                    userId: conv.myuserid,
                    convid: conv.id
                }).then(
                    function success(res) {
                        newText.msg = '';
                        conv.messages.push(res.data.message);
                        console.log(res.data.message);

                        scope.sendMessagePending = false;
                    },
                    function error(err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                        scope.sendMessagePending = false;
                    }   
                )
            }
            scope.deleteConversation = function(conv) {
                if (conv == undefined || conv == null) {
                    return;
                }

                $http.post('/conv', { 
                    convId: conv.id, 
                    aid: scope.acc.id
                }).then(
                    function success(res) {

                        // Keep content up to date!
                        var index = findIndexById(scope.acc.convs, conv.id);
                        if (index >= 0) {
                            scope.acc.convs.splice(index, 1);
                        }

                        // Notification on main panel
                        scope.ui.addNotif({
                            en: 'Conversation deleted.',
                            ch: '会话已删除。',
                            icon: 'tick'
                        })
                    },
                    function error(err) {
                        scope.ui.addNotif({
                            en: 'Unable to delete conversation at this time. Please try again later.',
                            ch: '目前无法删除对话。请稍后再试。',
                            icon: 'em'
                        })
                    }
                )
            }
            scope.changeTranslation = function (msg) {
                console.log('changing translation for msg:' + JSON.stringify(msg));

                if (scope.acc == undefined) {
                    scope.ui.onInactivityLogout();
                    return;
                }

                if (msg.id == undefined || msg.cid == undefined || 
                    msg.text == undefined || msg.text.src == undefined || msg.translation == undefined) {
                        return;
                    }

                $http.post('/conv/changetranslate', { 
                    aid: scope.acc.id,
                    cid: msg.cid,
                    mid: msg.id,
                    src: msg.text.src,
                    translation: msg.translation
                }).then(
                    function success (res) {

                        if (res.data.msg != undefined) {
                            var index = findIndexById(scope.selectedConv.messages, res.data.msg.id);
                            if (index >= 0) {
                                scope.selectedConv.messages[index] = res.data.msg;
                            }
                        }
                    },
                    function error (err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                    }
                )
            }
            // This is called every time inbox gets initialised
            scope.getUserInbox();
        }
    }
});