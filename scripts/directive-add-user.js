app.directive('gkAddUser', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'addUser.html',
        scope: {
            ui: '=',
            acc: '=',
            methods: '=',
            selectable: '=',
            selected: '='
        },
        link: function (scope) {

            scope.currentlyFocusedUser = undefined;
            scope.showProfilePhotoEditor = false;

            scope.$watch('currentlyFocusedUser.replaceDps.length', function (newVal, oldVal) {
                if (newVal != undefined && newVal > 0 && newVal > oldVal) {
                    scope.showProfilePhotoEditor = true;
                }
            });
            scope.profilePhotoCallback = function (action) {
                if (action == 'cancel' || action == 'capture') {
                    scope.showProfilePhotoEditor = false;
                }
            }

            scope.clickLogoBtn = function (user) {
                var el = document.getElementById('userLogoBtn_' + user.id);
                el.click();

                scope.currentlyFocusedUser = user;
            }
            scope.addUser = function () {
                if (scope.acc == null || scope.acc.id == null) {
                    return;
                }
                $http.post('/adduser', { aid: scope.acc.id }).then(
                    function success (res) {
                        res.data.user.isEditting = true;
                        scope.acc.users.unshift(res.data.user);
                    },
                    function error(err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                    }
                )
            }
            scope.updateUser = function (user) {

                // Client-side validation
                if (user.fname == null) {
                    user.modifyResult = {
                        en: 'Please enter your first name.',
                        ch: '请输入您的名字。'
                    };
                    return;
                }
                if (user.lname == null) {
                    user.modifyResult = {
                        en: 'Please enter your last name.',
                        ch: '请输入您的姓氏。'
                    }
                    return;
                }
                if (user.email == null) {
                    user.modifyResult = {
                        en: 'Please enter your email address.',
                        ch: '请输入您的电子邮件地址。'
                    };
                    return;
                }
                if (user.phone == null) {
                    user.modifyResult = {
                        en: 'Please enter your phone number.',
                        ch: '请输入您的电话号码。' 
                    }
                    return ;
                }
                
                // Loading display
                if (scope.updateUserPending) {
                    return;
                }
                scope.updateUserPending = true;
                
                if (user.replaceDps.length > 0) {
                    var image = user.replaceDps[user.replaceDps.length - 1].result;

                    var fd = new FormData();
                    var imgBlob = dataURItoBlob(image);
                    fd.append('file', imgBlob);
                    fd.append('aid', scope.acc.id);
                    fd.append('uid', user.id)
                    fd.append('filetype', 'dp');

                    $http.post(
                        '/files',
                        fd,
                        {
                            transformRequest: angular.identity,
                            headers: { 'Content-Type': undefined },
                            timeout: 30000
                        }
                    ).then(
                        function success() {
                            scope.postUpdateUser(user);
                        },
                        function error(err) {
                            if (err.status == 413) {
                                scope.ui.addNotif({
                                    en: 'The image is too big. Please try again with a smaller one!',
                                    ch: '您上传的图片太大了。请再试一次小一个！',
                                    icon: 'em'
                                });
                            }
                            else if (err.status == 401) {
                                scope.ui.onInactivityLogout();
                                return;
                            }
                            scope.updateUserPending = false;
                        }
                    )
                }
                else {
                    scope.postUpdateUser(user);
                }
            }
            scope.postUpdateUser = function (user) {

                $http.post('/updateuser', { 
                    aid: scope.acc.id, 
                    user: {             /// we need to do this because there are other objects attached to user 
                        id: user.id,
                        email: user.email,
                        fname: user.fname,
                        lname: user.lname,
                        dpid: user.dpid,
                        bio: user.bio,
                        phone: user.phone
                    }
                }).then(
                    function success (res) {
                        var index = findIndexById(scope.acc.users, res.data.user.id);
                        if (index >= 0) {
                            scope.acc.users[index] = res.data.user;
                        }
                        user.isEditting = false;

                        // send message 
                        scope.ui.addNotif({
                            en: 'Successfully changed user profile.',
                            ch: '成功更改了用户个人资料。',
                            icon: 'tick'
                        })

                        scope.updateUserPending = false;
                    },
                    function error (err) {
                        console.log(err);
                        if (err.status == 413) {
                            scope.ui.addNotif({
                                en: 'The image is too big. Please try again with a smaller one!',
                                ch: '您上传的图片太大了。请再试一次小一个！',
                                icon: 'em'
                            });
                        }
                        else {
                            scope.ui.addNotif({
                                en: 'Unable to change your user profile at this time. Please try again later.',
                                ch: '目前无法更改您的用户个人资料。请稍后再试。',
                                icon: 'em'
                            });
                        }
                        scope.updateUserPending = false;
                    }
                )
            }
            scope.deleteUser = function (user) {
                scope.ui.confirmAction(
                    {
                        en: 'Are you sure you wish to delete the user profile for "' + (user.fname + ' ' + user.lname).trim() + '"?',
                        ch: '您确定要删除此用户个人资料吗？'
                    },
                    {
                        en: 'Delete',
                        ch: '删除'
                    },
                    function () {
                        $http.post('/deleteuser', { aid: scope.acc.id, uid: user.id}).then(
                            function success (res) {
                                var index = findIndexById(scope.acc.users, user.id);
                                if (index >= 0) {
                                    scope.acc.users.splice(index, 1);
                                }

                                // user notification
                                scope.ui.addNotif({
                                    en: 'Successfully deleted user profile.',
                                    ch: '已成功删除用户个人资料。',
                                    icon: 'tick'
                                })
                            },
                            function error (err) {
                                if (err.status == 401) {
                                    scope.ui.onInactivityLogout();
                                    return;
                                }
                                scope.ui.addNotif({
                                    en: 'Unable to delete user profile at this time. Please try again later.',
                                    ch: '目前无法删除用户个人资料。请稍后再试。',
                                    icon: 'em'
                                })
                            }
                        )
                    }
                )
            }
            scope.setSelectedUser = function (user) {
                if (scope.selectable) scope.selected = user;
            }
        }
    }
});