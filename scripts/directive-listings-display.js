app.directive('gkListingsDisplay', function() {
    return {
        restrict: 'E',
        templateUrl: 'listingsDisplay.html',

        scope: {
            ui: '=',
            acc: '=',
            listings: '=',
            methods: '=',
            type: '='
        },
        link: function(scope, element, attrs) {

            scope.selectedListing = undefined;
 
            // This is passed down to directive-user-listing-panel
            scope.lmethods = {
                removeListing: function (listingId) {

                    var index = findIndexById(scope.listings, listingId);
                    if (index >= 0) {

                        scope.listings.splice(index, 1);

                        if (scope.listings.length > index) {
                            scope.selectedListing = scope.listings[index];
                        }
                        else if (scope.listings.length > 0) {
                            scope.selectedListing = scope.listings[0];
                        }
                        else {
                            scope.selectedListing = undefined;
                        }
                    }
                }
            }

            scope.orderings = [
                { en: 'Newest first', ch: '最新到最旧' },
                { en: 'Oldest first', ch: '最旧到最新' }
            ]

            // resize details div on page load
            angular.element(function () {

                console.log('resizing detailed panel on load');
                scope.ui.resizeDetailedPanel();

                if (scope.listings != undefined && scope.listings.length > 0) {
                    console.log('setting selected listing as 0');
                    scope.selectedListing = scope.listings[0];
                }
                else {
                    console.log('WARNING: UNABLE TO SET SELECTED LISTING!!!');
                    console.log(scope.listings);
                }
                scope.$apply();
            });

            scope.$watch('listings', function () {
                if (scope.listings != undefined && 
                    scope.listings.length > 0 && 
                    scope.selectedListing == undefined) 
                {
                    scope.selectedListing = scope.listings[0];
                }
            })

            scope.gui = {
                controls: {
                    showDetailedPanel: function (listing) {
                        console.log('showing listing: ' + listing);
                        scope.selectedListing = listing;
                    }
                }
            }
        }
    }
});