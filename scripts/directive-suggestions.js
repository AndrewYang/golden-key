app.directive('gkSuggestions', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'suggestions.html',
        scope: {
            listening: '=', // whether we are listening to a change
            query: '=', // the variable being watched
            chosen: '=',
            theme: '='
        },
        link: function (scope, element, attrs) {

            var typeTimer = undefined;
            scope.suggestions = [];
            scope.changeOccurred = false;

            scope.$watch('query', function () {

                if (!scope.listening) {
                    return;
                }
                
                if (scope.query == undefined || scope.query.trim().length == 0) {
                    scope.changeOccurred = false;
                    return;
                }

                // Only act if the query is different to currently chosen
                if (scope.chosen == undefined ||
                    scope.chosen.s.toUpperCase().trim() != scope.query.toUpperCase().trim()) {
                    scope.changeOccurred = true;
                    scope.getSuburbSuggestions(scope.query);
                }
                else {
                    scope.changeOccurred = false;
                }
            });

            scope.getSuburbSuggestions = function (query) {

                // doing to to avoid too many queries
                clearTimeout(typeTimer);
                typeTimer = setTimeout(function () {

                    $http.post('/suburbsuggestion', { q: query }).then(
                        function success(res) {
                            scope.suggestions = res.data.matches;
                        },
                        function err(err) {
                            console.log(err);
                        }
                    );
                },
                    400);
            };

            scope.choose = function (suggestion) {
                console.log('choosing: ' + suggestion);
                scope.chosen = suggestion;
                scope.changeOccurred = false;
            }
        }
    }
});