app.directive('gkListingPanel', function($interval) {
    return {
        restrict: 'E',
        templateUrl: 'listingPanel.html',
        scope: {
            acc: '=',
            ui: '=',
            listing: '=',
            methods: '=',
            controls: '='
        },
        link: function(scope, element, attrs) {

            // Get the right amount of features
            var nFeatures = undefined;
            var cachedFeatures = undefined;
            scope.getFeatures = function (features) {
                if (nFeatures === scope.ui.maxFeatures) return cachedFeatures;
                
                nFeatures = scope.ui.maxFeatures;
                cachedFeatures = features.slice(0, nFeatures);
                return cachedFeatures;
            }

            scope.isnew = function (listing) {
                if (listing.ltype == 'Lease' || listing.ltype == 'Sell') {
                    var currTime = (new Date()).getTime();
                    var days = (currTime - listing.postingdate) / 1000 / 60 / 60 / 24;
                    if (days < 7.0) return true;
                }
                return false;
            }

            scope.formatDate = function (time) {
                return new Date(time).toLocaleDateString("en-AU", { weekday: 'short', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric' });
            }

            var timer;
            var galleryIndex = 1;
            scope.startGalleryAnimation = function () {

                if (scope.listing == undefined || timer != undefined) return;
                if (scope.listing.imageIds.length <= 1) return;   // no point to the animation

                scope.listing.hidePreloader = true;               // Hide the loader
                timer = $interval(function () {
                    loadImageAsync('/images/' + scope.listing.imageIds[galleryIndex % scope.listing.imageIds.length].i);
                    galleryIndex++;
                }, 2000);
            }
            scope.stopGalleryAnimation = function () {
                
                if (scope.listing == undefined) return;
                if (scope.listing.imageIds.length <= 1) return;     // the animation never began

                scope.listing.hidePreloader = false;

                if (timer != undefined) {
                    $interval.cancel(timer);
                    timer = undefined;
                }

                loadImageAsync('/images/' + scope.listing.imageIds[0].i);
                galleryIndex = 1;
            }
            
            var imgProxy = new Image();
            var isLoading = false;
            imgProxy.onload = function() {
                isLoading = false;
                scope.listing.currDisplayedImage = imgProxy.src;
                scope.$apply();
            };
            function loadImageAsync(src) {
                if (!isLoading) {
                    isLoading = true;
                    imgProxy.src = src;
                }
            }

            scope.getFeatureDisplayUnits = function (feature) {

                if (feature == null) return '';
                
                switch (feature.type) {
                    case 'LandArea': 
                    case 'BuildingArea':
                        return scope.ui.en ? 'm²' : '平方米';
                    case 'Frontage':
                        return scope.ui.en ? 'm' : '米';
                    case 'Storys':
                        return scope.ui.en ? 'Floors' : '层楼';
                    case 'Zoning':
                        return scope.ui.en ? 'km' : '公里';
                }
                return ''
            };
            scope.getListingType = function (listing) {
                // for the top listing panel
                if (listing == undefined) return '';

                if (listing.ltype == 'Sell') {
                    return scope.ui.en ? 'For sale' : '出 售';
                }
                if (listing.ltype == 'Lease') {
                    return scope.ui.en ? 'For lease' : '出 租';
                }
                if (listing.ltype == 'Sold') {
                    return scope.ui.en ? 'Sold' : '已 售';
                }
                if (listing.ltype == 'Under offer') {
                    return scope.ui.en ? 'Under Offer' : '低于报价';
                }
                return listing.ltype;
            }

            // wishlist
            scope.inWishlist = function (listingId) {
                if (scope.acc == null || scope.acc.wishlist == null) {
                    return false;
                }

                var wl = scope.acc.wishlist;
                for (var i = 0; i < wl.length; i++) {
                    if (wl[i] == listingId) return true;
                }
                return false;
            }

            scope.$on('$destroy', function() {
                if (timer != undefined) {
                    $interval.cancel(timer);
                    timer = undefined;
                }
            });
        }
    }
});