app.directive('gkUserListingPanel', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'userListingPanel.html',
        scope: {
            ui: '=',
            acc: '=',
            listing: '=',
            methods: '=',
            lmethods: '=',
            gui:'=',
            type: '='
        },
        link: function(scope, element, attrs) {
           
            scope.confirmRibbonChange = function (listing, ribbon) {

                if (listing == undefined) return;

                var msg;
                if (ribbon == 'Sold') {
                    msg = { 
                        en: "Congratulations! Please confirm that you're marking '" + listing.address.street + " as 'Sold'.",
                        ch: "恭喜！请确认您将此房产标记为'已售出'"
                    };
                }
                else {
                    msg = {
                        en: "Congratulations! Please confirm that you're marking '" + listing.address.street + " as 'Under Offer'.",
                        ch: "恭喜！请确认您将此房产标记为'优惠'"
                    };
                }

                scope.ui.confirmAction(
                    msg,
                    { en: 'Confirm', ch: '确认' },
                    function () {
                        console.log('posting mark listing');
                        $http.post('/marklisting', {
                            aid: scope.acc.id,
                            lid: listing.id,
                            ribbon: ribbon
                        }).then(
                            function success(res) {
                                listing.ltype = ribbon; // for UI sake

                                if (ribbon == 'Sold') {
                                    scope.ui.addNotif({
                                        en: 'Congratulations! Your listing has been marked as sold.',
                                        ch: '恭喜！您的商家信息已标记为已售出',
                                        icon: 'tick'
                                    })
                                }
                                else {
                                    scope.ui.addNotif({
                                        en: 'Congratulations! Your listing has been marked as Under Offer.',
                                        ch: '恭喜！您的商家信息已标记为已提供',
                                        icon: 'tick'
                                    })
                                }
                            },
                            function error(err) {
                                if (err.status == 401) {
                                    scope.ui.onInactivityLogout();
                                    return;
                                }
                            }
                        )
                    },
                    'fireworks'
                );
            }
            scope.editUserListing = function(listing) {
                // populate the 'post listings' page with the current data 
                scope.ui.populateListingForm(listing, 'edit');
                scope.ui.currMainDisplay = 'postlisting';
            }
            scope.deleteUserListing = function(listing) {

                scope.ui.confirmAction(
                    {
                        en: 'Are you sure you wish to delete the listing for "' + (listing.address.street + ', ' + listing.address.suburb).trim() + '"?',
                        ch: '您确定要删除此商家信息吗？'
                    },
                    {
                        en: 'Delete',
                        ch: '删除'
                    },
                    function () {
                        scope.postDeleteListing(listing);
                    }
                );
            }
            scope.postDeleteListing = function (listing) {
                $http.post(
                    '/deletelisting',
                    {
                        aid: scope.acc.id,
                        listingId: listing.id
                    }
                ).then(
                    function success(res) {
                        if (res.data.success) {
                            scope.lmethods.removeListing(res.data.removedId);
                            scope.ui.addNotif({
                                en: 'Success! Your listing has been deleted.',
                                ch: '您的商家信息已被删除。',
                                icon: 'tick'
                            });
                        }
                        else {
                            scope.ui.addNotif({
                                en: 'Unable to delete your listing at this time. Please try again later.',
                                ch: '目前无法删除您的商家信息。请稍后再试。',
                                icon: 'em'
                            });
                        }
                    },
                    function error(err) {
                        
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                        scope.ui.addNotif({
                            en: 'Unable to delete your listing at this time. Please try again later.',
                            ch: '目前无法删除您的商家信息。请稍后再试。',
                            icon: 'em'
                        });
                    }
                )
            }
            
            // for wishlist 
            scope.removeWishlistCallback = function (res) {
                // callback function 
                if (res.status == 200) {
                    scope.lmethods.removeListing(res.data.lid);
                }
            }

            scope.getDateString = function (time) {
                return new Date(time).toLocaleString();
            }
        }
    }
});