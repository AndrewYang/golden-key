app.directive('gkLogin', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'login.html',

        link: function(scope, element, attrs) {

            scope.newuser = {}; // for the signup
            scope.lui = {};

            scope.resetLoginForm = function() {
                scope.ui.login.signupResult.msg = {};
                scope.ui.login.result.msg = {};
            }

            scope.signup = function (user) {

                if (user == null) {
                    scope.ui.login.signupResult.msg = {
                        en: "Please fill in the form to create an account.",
                        ch: "请填写表格以创建帐户"
                    };
                    return;
                }
                if (user.email == null) {
                    scope.ui.login.signupResult.msg = {
                        en: "Please enter your email address.",
                        ch: '请输入您的电子邮件地址'
                    };
                    return;
                }
                // Only the first name is necessary
                if (user.firstname == null) {
                    scope.ui.login.signupResult.msg = {
                        en: "Please enter your first name.",
                        ch: '请输入你的名字'
                    };
                    return;
                }
                if (user.pass == null) {
                    scope.ui.login.signupResult.msg = {
                        en: "Please enter a strong password.",
                        ch: '请输入密码'
                    };
                    return;
                }

                scope.lui.signingIn = true;

                // TODO: Check if email address is valid
                $http.post(
                    '/signup',
                    {
                        e: user.email,
                        p: user.pass,
                        fname: user.firstname,
                        lname: user.lastname
                    }
                ).then(
                    function success(res) {
                        scope.onSuccessfulLogin(res);
                        scope.ui.addNotif({
                            en: 'Account successfully created. Welcome! You are now logged in.',
                            ch: '成功建立帐户。欢迎！您现在已登录',
                            icon: 'tick'
                        });
                        scope.lui.signingIn = false;
                    },
                    function error(res) {
                        if (res.error != null) {
                            scope.ui.login.signupResult.msg = {
                                en: res.error,
                                ch: res.cherror
                            }
                        }
                        else if (res.status == 409) {
                            scope.ui.login.signupResult.msg = {
                                en: 'Your email address is already being used by another account. Please log in or choose another email address.',
                                ch: '您的电子邮件地址已被其他帐户使用。请登录或选择其他电子邮件地址。'
                            };
                        }
                        else if (res.status == 400 || res.status == 401) {
                            scope.ui.login.signupResult.msg = {
                                en: 'Invalid email or password. Please choose a different one and try again.',
                                ch: '无效的电子邮件或密码。请选择其他的，然后重试。'
                            };
                        }
                        else if (res.status == 500) {
                            scope.ui.login.signupResult.msg = {
                                en: "Unfortunately, something has gone wrong. Please try again later.",
                                ch: '此时服务不可用。请稍后再试。'
                            };
                        }
                        scope.lui.signingIn = false;
                    }
                );
            };
            scope.login = function (user) {
                if (user == null) {
                    scope.ui.login.result.msg = {
                        en: "Please enter your login details.",
                        ch: '请输入您的登录信息'
                    };
                    return;
                }
                if (user.email == null || user.email.length == 0) {
                    scope.ui.login.result.msg = {
                        en: "Please enter your email.",
                        ch: '请输入您的电子邮件'
                    };
                    return;
                }
                if (user.pass == null || user.pass.length == 0) {
                    scope.ui.login.result.msg = {
                        en: "Please enter your password.",
                        ch: '请输入您的密码'
                    };
                    return;
                }
        
                scope.lui.loggingIn = true;
                $http.post(
                    '/login', { useToken: false, e: user.email, p: user.pass }
                ).then(
                    function success(res) {

                        // Only do this on non-token logins
                        _idleSeconds = 0;

                        scope.onSuccessfulLogin(res);
                        scope.ui.addNotif({
                            en: 'You are now logged in.',
                            ch: '您现在已登录。',
                            icon: 'tick'
                        });
                        scope.lui.loggingIn = false;
                    },
                    function error(res) {
                        if (res.status == 400 || res.status == 401) {
                            scope.ui.login.result.msg = {
                                en: "Incorrect email or password. Please try again.",
                                ch: '错误的邮箱帐号或密码。请再试一次。'
                            }
                            scope.ui.login.result.isIncorrectPassword = true;
                        }
                        else if (res.status == 500) {
                            scope.ui.login.result.msg = {
                                en: "Unfortunately, something has gone wrong. Please try again later.",
                                ch: '此时服务不可用。请稍后再试。'
                            };
                            scope.ui.login.result.isIncorrectPassword = false;
                        }
                        scope.lui.loggingIn = false;
                    }
                )
        
                // wipe user entries from local memory
                if (scope.loginuser != null) {
                    scope.loginuser.email = null;
                    scope.loginuser.pass = null;
                    scope.loginuser = null;
                }
            };

            // this is called twice every page refresh - TODO
            scope.onSuccessfulLogin = function (res) {
                console.log(res);
                scope.acc = res.data;   // account includes user object
                scope.ui.topElement = null; // get rid of the login form

                var exp = res.data.sessionExpiry;
                var now = (new Date()).getTime();
                scope.sessionTimer = setTimeout(scope.queryUserIdleEvent, (exp - now) * 0.9);
            };
            scope.queryUserIdleEvent = function () {
                
                if (_idleSeconds < 15 * 60) {
                    // if the user is not idle, then post a ajax request to extend the session
                    scope.loginWithToken();
                }
                else {
                    // Log the user out
                    scope.logout();
                }
            }
        
            scope.logout = function () {
                $http.post(
                    '/logout', {}
                ).then(
                    function success() {
                        scope.onSuccessfulLogout();
                    },
                    function error(res) {
                        scope.ui.addNotif({
                            en: 'Unable to log out at this time. Please try again later.',
                            ch: '目前无法退出。请稍后再试。',
                            icon: 'em'
                        });
                    }
                )
            };
            scope.onSuccessfulLogout = function () {
                // Reset page
                scope.ui.currMainDisplay = 'listings';
        
                // Reset all parameters
                if (scope.sessionTimer != null) {
                    clearTimeout(scope.sessionTimer);
                }
                scope.acc = null;
                scope.userlistings = [];
                scope.queryParams = null;
                scope.clearListingForm();
    
                scope.ui.addNotif({
                    en: 'You are now logged out. See you next time!',
                    ch: '您现在已退出。下次见！',
                    icon: 'tick'
                });
            },
        
            scope.displayLogin = function (showForm, isLogin) {
                var login = document.getElementById('loginForm');
                if (showForm) {
                    login.classList.remove("hidden");
        
                    if (isLogin) {
                        document.getElementById('loginForm-signup').style.display = 'none';
                        document.getElementById('loginForm-login').style.display = 'block';
                    }
                    else {
                        document.getElementById('loginForm-login').style.display = 'none';
                        document.getElementById('loginForm-signup').style.display = 'block';
                    }
                }
                else {
                    login.classList.add('hidden');
                }
            };
            
            scope.loginWithToken = function() {
                $http.post(
                    '/login', { useToken: true }
                ).then(
                    function success(res) {
                        scope.onSuccessfulLogin(res);
                    },
                    function error(res) {
                        scope.acc = null;
                    }
                )
            }

            scope.sendPasswordRecoveryEmail = function (param) {

                if (param == undefined) {
                    param = {};
                }

                if (!isEmail(param.email)) {
                    param.msg = {
                        en: 'Please enter a valid email address.',
                        ch: '请输入有效的电子邮件地址。'
                    };
                    return;
                }

                // success 
                param.msg = undefined;
                scope.ui.login.recoveryEmailSending = true;

                $http.post('/recoverpass', { e : param.email }).then(
                    function success (res) {
                        scope.ui.login.recoveryEmailSending = false;
                        scope.ui.login.recoveryEmailSent = true;
                    },
                    function error (err) {
                        scope.ui.login.recoveryEmailSending = false;

                        // Too many requests per hour
                        if (err.status == 403) {
                            param.msg = {
                                en: 'You have reset your account password too many times recently. Please try again later, or contact us for help.',
                                ch: '您最近重置了多次帐户密码。请稍后再试，或与我们联系以获取帮助。'
                            };
                        }
                        else {
                            param.msg = {
                                en: 'Unfortunately, we have encountered an error. Please try again.',
                                ch: '不幸的是，我们遇到了一个错误。请再试一次。'
                            };
                        }
                    }
                )
            }

            scope.submitPassword = function (param) {

                if (param == undefined) {
                    param = {};
                }

                if (!isEmail(param.email)) {
                    param.msg = {
                        en: 'Please enter your email address.',
                        ch: '请输入有效的电子邮件地址。'
                    };
                    return;
                }

                if (param.pass == undefined || param.pass.length == 0) {
                    param.msg = {
                        en: 'Please enter a password.',
                        ch: '请输入一个密码。'
                    };
                    return;
                }

                if (param.code == undefined || param.code.length != 6) {
                    param.msg = {
                        en: 'Please enter a valid password recovery code.',
                        ch: '请输入有效代码。'
                    };
                    return;
                }

                scope.ui.login.recoveryEmailSending = true;
                $http.post('/resetpass', {
                    e: param.email,
                    p: param.pass,
                    c: param.code
                }).then(
                    function success (res) {
                        scope.onSuccessfulLogin(res);
                        scope.ui.addNotif({
                            en: 'Your password has been reset. Welcome back!',
                            ch: '您的密码已重置。欢迎回来！',
                            icon: 'tick'
                        });
                        scope.lui.loggingIn = false;
                        scope.ui.login.recoveryEmailSending = false;
                    },
                    function error (err) {
                        if (err.status == 401) {
                            param.email = undefined;
                            param.pass = undefined;
                            param.code = undefined;
                            param.msg = {
                                en: 'The password recovery code you entered is incorrect. To protect your account, we have deactivated your recovery code. Please start again.',
                                ch: '您输入的密码恢复代码不正确。为了保护您的帐户，我们停用了恢复代码。请再试一次。'
                            };
                            scope.ui.login.recoveryEmailSent = false;
                        }
                        else {
                            param.msg = {
                                en: 'An error has occurred, please try again.',
                                ch: '发生了错误。请再试一次。'
                            };
                        }

                        scope.ui.login.recoveryEmailSending = false;
                    }
                )
            }

            // Log in with token - we have no way of knowing whether the token 
            // is there using javascript, so we just have to try this
            scope.loginWithToken();
        }
    }
});