app.directive('gkBlocksPreloader', function($interval) {
    return {
        restrict: 'E',
        templateUrl: "blocksPreloader.html",
        scope: {
            foreColor: '=',
            backColor: '='
        },
        link: function(scope, element, attrs) {

            var b = 28, g = 7;
            scope.blockSize = b + '%';

            scope.blocks = [
                { top: 0,               left: 0,                                color: scope.backColor },
                { top: 0,               left: (b + g) + '%',                    color: scope.backColor },
                { top: 0,               left: (2 * b + 2 * g) + '%',            color: scope.backColor },

                { top: (b + g) + '%',           left: (2 * b + 2 * g) + '%',    color: scope.backColor },
                
                { top: (2 * b + 2 * g) + '%',   left: (2 * b + 2 * g) + '%',    color: scope.backColor },
                { top: (2 * b + 2 * g) + '%',   left: (b + g) + '%',            color: scope.backColor },
                { top: (2 * b + 2 * g) + '%',   left: 0,                        color: scope.backColor },

                { top: (b + g) + '%',           left: 0,                        color: scope.backColor },
            ];

            var previous = -1, next = 0;
            var timer = $interval(function () {

                previous = (previous + 1) % scope.blocks.length;
                next = (next + 1) % scope.blocks.length;

                scope.blocks[previous].color = scope.backColor;
                scope.blocks[next].color = scope.foreColor;
            }, 120);

            scope.$on('$destroy', function() {
                if (timer != undefined) {
                    $interval.cancel(timer);
                    timer = undefined;
                }
            });
        }
    }
});