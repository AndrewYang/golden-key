app.directive('gkSearch', function($http, $timeout) {
    return {
        restrict: 'E',
        templateUrl: 'search.html',

        link: function(scope, element, attrs) {
            
            // Whether the button is busy
            scope.mainSearching = false;

            scope.searchparams = {
                ltypes: [],
                types: [],
                suburbs: [],
                postcodes: [],
                states: [],
                
                static: {
                    getPriceIncrement: function() {
                        if (scope.searchparams.op == 'Lease') {
                            return 50;
                        }
                        return 50000;
                    },
                    landSizeIncrement: 100,
                },

                op: scope.static.listingTypes[0].k,
                includenew: true,
                includeold: true,
                currentState: scope.static.states[0].en,
                currentType: scope.static.propertyTypes[0].en
            };

            scope.$watch('searchparams.chosenSuburb', function () {
                if (scope.searchparams.chosenSuburb != undefined) {
                    scope.searchparams.suburbs.push(scope.searchparams.chosenSuburb.s.toUpperCase());
                    scope.searchparams.currentSuburb = '';
                }
            });

            scope.search = function (p) {

                if (scope.mainSearching) {
                    return;
                }

                if (scope.ui.isMobileDisplay()) {
                    var searchBar = document.getElementById('searchBar');
                    searchBar.style.height = scope.ui.headerHeight;
                    scope.ui.shown.mobileSearch = false;
                }
                // for convenience
                scope.ui.currMainDisplay = 'listings';
                
                // Client validation here

                // the actual search query object
                var q = {}; 
                if (p.includenew && !p.includeold) {
                    q.isnew = true;
                }
                else if (p.includeold && !p.includenew) {
                    q.isnew = false;
                }
                
                if (p.op != null) {
                    q.ltypes = [ p.op ]; // Everything is 1 except for sold, which also contains under offer
                    if (p.op == 'Sold') {
                        q.ltypes.push('Under offer');
                    }
                }
                
                // The type of property, e.g. house
                if (p.types != undefined && p.types.length > 0) {
                    q.types = p.types;
                }
                if (q.currentType != undefined && q.currentType.length > 0) {
                    if (q.types != undefined) q.types.push(p.currentType);
                    else q.types = [p.currentType];
                }

                // Suburb
                if (p.suburbs != undefined && p.suburbs.length > 0) {
                    q.suburbs = p.suburbs;
                }
                if (p.currentSuburb != undefined && p.currentSuburb.length > 0) {
                    if (q.suburbs != undefined) q.suburbs.push(p.currentSuburb);
                    else q.suburbs = [p.currentSuburb];
                }

                // Post code
                if (p.postcodes != undefined && p.postcodes.length > 0) {
                    q.postcodes = p.postcodes;
                }
                if (p.currentPostcode != undefined) {
                    if (q.postcodes != undefined) q.postcodes.push(p.currentPostcode);
                    else q.postcodes = [p.currentPostcode];
                }

                // States
                if (p.states != undefined && p.states.length > 0) {
                    q.states = p.states;
                }
                if (p.currentState != undefined && p.currentState.length > 0) {
                    if (q.states != undefined) {
                        // only add state if it's not contained in list already
                        if (q.states.indexOf(p.currentState) < 0) {
                            q.states.push(p.currentState);
                        }
                    }
                    else q.states = [p.currentState];
                }
                if (q.states != undefined && q.states.indexOf("All") >= 0) { // having all in there means there is nothing
                    q.states = undefined;
                }

                // Beds - server side validation only
                if (p.minbeds != undefined && p.minbeds >= 0) {
                    q.minbed = p.minbeds;
                }
                if (p.maxbeds != undefined && p.maxbeds >= 0) {
                    q.maxbed = p.maxbeds;
                }

                // Baths - server side validation only
                if (p.minbaths != undefined && p.minbaths >= 0) {
                    q.minbath = p.minbaths;
                }
                if (p.maxbaths != undefined && p.maxbaths >= 0) {
                    q.maxbath = p.maxbaths;
                }

                // Garages - server side validation only
                if (p.mingarages != undefined && p.mingarages >= 0) {
                    q.mincar = p.mingarages;
                }
                if (p.maxgarages != undefined && p.maxgarages >= 0) {
                    q.maxcar = p.maxgarages;
                }

                // Land size - server side validation only
                if (p.minlandsize != undefined && p.minlandsize >= 0) {
                    q.minlandsize = p.minlandsize;
                }
                if (p.maxlandsize != undefined && p.maxlandsize >= 0) {
                    q.maxlandsize = p.maxlandsize;
                }

                // Building size - server side validation only 
                if (p.minbuildingsize != undefined && p.minbuildingsize >= 0) {
                    q.minbuildingsize = p.minbuildingsize;
                }
                if (p.maxbuildingsize != undefined && p.maxbuildingsize >= 0) {
                    q.maxbuildingsize = p.maxbuildingsize;
                }

                // Price
                if (p.maxprice != undefined && p.maxprice >= 0) {
                    q.maxprice = p.maxprice;
                }
                if (p.minprice != undefined && p.minprice >= 0) {
                    q.minprice = p.minprice;
                }

                q.from = 0;
                q.extrafrom = 0;
                q.orderby = scope.orderParams.orderBy;

                // Remove from view 
                scope.listings = [];
                scope.ui.panels = [];
                
                // ui changes
                scope.mainSearching = true;

                console.log(JSON.stringify(p));
                console.log(JSON.stringify(q));

                // post 
                $http.post('/search', q).then(
                    function success(res) {
                        console.log(res.data);
                        scope.ui.displayListings(res.data, true);
                        scope.mainSearching = false;
                    },
                    function error(res) {
                        scope.ui.listingDataLoaded = true;
                        scope.mainSearching = false;
                    }
                )
            };
            
            scope.$watch('searchparams.op', function () {

                // if user switches from rent to buy - 
                if (scope.searchparams.op == 'Sell' && scope.searchparams.minprice != undefined) {
                    scope.searchparams.minprice = 0;    // should propagate
                }
            });
            scope.$watch('searchparams.minprice', function() {
                scope.searchparams.maxprice = scope.searchparams.minprice + scope.searchparams.static.getPriceIncrement();
            });
            scope.$watch('searchparams.minlandsize', function() {
                scope.searchparams.maxlandsize = scope.searchparams.minlandsize + scope.searchparams.static.landSizeIncrement;
            });
            scope.$watch('searchparams.minbuildingsize', function() {
                scope.searchparams.maxbuildingsize = scope.searchparams.minbuildingsize + scope.searchparams.static.landSizeIncrement;
            });
            scope.checkEnter = function (event) {
                var keyCode = event.which || event.keyCode;
                if (keyCode === 13) {
                    scope.searchparams.postcodes.push(scope.searchparams.currentPostcode);
                    scope.searchparams.currentPostcode = undefined;
                }
            };
            scope.hideSearchbar = function () {
                var nSteps = 15;
                var originalSize = scope.ui.size.contentBarLeft;
                var step = scope.ui.size.contentBarLeft / nSteps;

                for (var i = 1; i <= nSteps; i++) {
                    $timeout(function (n) {
                        scope.ui.size.contentBarLeft = originalSize - n * step;
                        scope.ui.size.searchBarLeft = -n * step;
                    }, i * 5, true, i);
                }

                $timeout(function () {
                    scope.ui.shown.searchBar = false;
                    recalculateScopeParameters(scope);
                    repaintBody(scope);
                }, (nSteps + 1) * 5);
            }
            scope.showSearchBar = function () {
                
                scope.ui.shown.searchBar = true;
                recalculateScopeParameters(scope);
                repaintBody(scope);
            }

            // onload
            scope.search(scope.searchparams);
        }
    }
});