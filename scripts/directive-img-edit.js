
app.directive('gkCanvasImgEditor', function () {
    return {
        restrict: 'E',
        templateUrl: 'imgEdit.html',

        scope: {
            gkSelectedIndex: '=',
            gkImgs: '=',
            aspectRatio: '=',
            callback: '='
        },
        link: function (scope) {

            scope.workarea = angular.element(document.querySelector('#img-edit-canvas'))[0];
            scope.img = angular.element(document.querySelector('#img-edit-canvas-main'))[0];
            scope.imgObj = new Image();
            scope.imgObj.onload = function() {
                scope.scaleImg();
            };

            scope.scaleImg = function() {
                var imgSize = {
                    height: scope.imgObj.height,
                    width: scope.imgObj.width
                };

                // height is always set as the same as the mask, width set as same aspect ratio
                scope.gkPosition.height = scope.mask.height;
                scope.gkPosition.width = imgSize.width / imgSize.height * scope.mask.height;

                // y is always set as the same as mask, x calculated so that the 
                var xLeeway = scope.gkPosition.width - scope.mask.width;
                scope.gkPosition.top = scope.mask.y;
                scope.gkPosition.left = (scope.mask.x - xLeeway / 2);

                scope.setImgPosition(scope.gkPosition);
            }

            // actual functionality starts here
            scaleWorkArea(scope.workarea);
            scope.mask = 
                displayMask(
                    document.getElementById('img-edit-canvas-mask-top'),
                    document.getElementById('img-edit-canvas-mask-right'),
                    document.getElementById('img-edit-canvas-mask-bottom'),
                    document.getElementById('img-edit-canvas-mask-left'),
                    {
                        height: scope.workarea.clientHeight,
                        width: scope.workarea.clientWidth
                    },
                    scope.aspectRatio
                );

            // the position of the image is stored here
            scope.gkPosition = {
                top: scope.mask.y, 
                left: scope.mask.x, 
                width: 0, 
                height: 0
            };

            scope.$watch("gkSelectedIndex", function () {
                if (scope.gkSelectedIndex >= 0) {
                    if (scope.gkImgs.length > scope.gkSelectedIndex) {
                        scope.imgObj.src = scope.gkImgs[scope.gkSelectedIndex].result;
                    }
                }
            })

            scope.resizeImg = function (delta) {

                var initialWidth = scope.imgObj.width;
                var initialHeight = scope.imgObj.height;

                var yChange = delta * 10;
                scope.gkPosition.height = scope.img.height + 2 * yChange;
                scope.gkPosition.width = scope.gkPosition.height * initialWidth / initialHeight;
                scope.gkPosition.top -= yChange;
                scope.gkPosition.left -= (scope.gkPosition.width - scope.img.width) / 2;
                scope.setImgPosition(scope.gkPosition);
            }
            scope.moveImg = function (direction) {
                var step = 10;
                if (direction == 'up') {
                    scope.gkPosition.top -= step;
                }
                else if (direction == 'left') {
                    scope.gkPosition.left -= step;
                }
                else if (direction == 'right') {    
                    scope.gkPosition.left += step;
                }
                else if (direction == 'down') {
                    scope.gkPosition.top += step;
                }
                scope.setImgPosition(scope.gkPosition);
            }
            scope.setImgPosition = function (position) {
                /* Sets the position of the image based on gkPosition */

                scope.img.style.top = position.top + 'px';
                scope.img.style.left = position.left + 'px';
                scope.img.style.height = position.height + 'px';
                scope.img.style.width = position.width + 'px';
            }
            scope.captureImg = function() {
                
                if (scope.gkSelectedIndex < 0) {
                    console.log('no image selected');
                    return;
                }

                var originalSize = {
                    height: scope.imgObj.height,
                    width: scope.imgObj.width
                };
                var ratio = scope.img.clientHeight / originalSize.height;

                // (x, y) starting coord on the src image itself
                var cropX = (scope.mask.x - scope.gkPosition.left) / ratio;
                var cropY = (scope.mask.y - scope.gkPosition.top) / ratio;
                
                var cropHeight = scope.mask.height / ratio;
                var cropWidth = scope.mask.width / ratio;

                if (scope.gkEdittedImgs == null) {
                    scope.gkEdittedImgs = [];
                }
                // setting gkSelectedImg!
                var cropped = cropImg(scope.imgObj, cropX, cropY, cropWidth, cropHeight);
                if (cropped) {

                    // reset the image
                    var file = scope.gkImgs[scope.gkSelectedIndex];
                    file.result = cropped;
                    if (file.origin == 's') {
                        file.isReplaced = true;
                    }

                    // hide image
                    scope.img.style.height = 0;
                    scope.img.style.width = 0;

                    scope.gkSelectedIndex = -1;
                }
                scope.callback('capture');
            }
            scope.resetImg = function() {
                scope.scaleImg();
                scope.callback('reset');
            }
            scope.cancelImg = function() {
                scope.gkSelectedIndex = -1;
                scope.callback('cancel');
            }
        }
    }
});

function scaleWorkArea(canvas) {
    canvas.style.height = (canvas.clientWidth * 0.8) + 'px';
}
function displayMask(top, right, bottom, left, canvasSize, aspectRatio) {

    // we want the mask to have aspect ratio height:width = 600:800
    // returns the mask dimensions as a rect
    var openHeight = canvasSize.height * 0.6;
    var openWidth = openHeight / aspectRatio;
    var verticalMargin = (canvasSize.height - openHeight) / 2, 
        horizontalMargin = (canvasSize.width - openWidth) / 2;

    var x1 = Math.round(verticalMargin), x2 = Math.round(canvasSize.height - verticalMargin);

    top.style.top = 0;
    top.style.left = 0;
    top.style.height = x1 + 'px';
    top.style.width = '100%';

    right.style.top = x1 + 'px';
    right.style.right = 0;
    right.style.height = (x2 - x1) + 'px';
    right.style.width = horizontalMargin + 'px';

    bottom.style.top = x2 + 'px';
    bottom.style.left = 0;
    bottom.style.height = (canvasSize.height - x2) + 'px';
    bottom.style.width = '100%';

    left.style.top = x1 + 'px';
    left.style.left = 0;
    left.style.height = (x2 - x1) + 'px';
    left.style.width = horizontalMargin + 'px';

    return {
        x: horizontalMargin,
        y: verticalMargin,
        height: canvasSize.height - 2 * verticalMargin,
        width: canvasSize.width - 2 * horizontalMargin
    }
}
function cropImg(imgObj, startX, startY, newWidth, newHeight) {

    var tnCanvas = document.createElement('canvas');
    var tnCanvasContext = tnCanvas.getContext('2d');
    tnCanvas.width = newWidth; tnCanvas.height = newHeight;

    var bufferCanvas = document.createElement('canvas');
    var bufferContext = bufferCanvas.getContext('2d');
    bufferCanvas.width = imgObj.width;
    bufferCanvas.height = imgObj.height;
    bufferContext.drawImage(imgObj, 0, 0);

    tnCanvasContext.drawImage(bufferCanvas, startX, startY, newWidth, newHeight, 0, 0, newWidth, newHeight);
    return tnCanvas.toDataURL();
}

app.directive('gkEditable', ['$document', function ($document) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {

            var startX, startY, initialMouseX, initialMouseY;

            // mouse drag event
            elm.bind('mousedown', function ($event) {
                startX = elm.prop('offsetLeft');
                startY = elm.prop('offsetTop');
                initialMouseX = $event.clientX;
                initialMouseY = $event.clientY;
                $document.bind('mousemove', mousemove);
                $document.bind('mouseup', mouseup);
                return false;
            });
            function mousemove($event) {
                var dx = $event.clientX - initialMouseX;
                var dy = $event.clientY - initialMouseY;

                scope.gkPosition.top = startY + dy;
                scope.gkPosition.left = startX + dx;
                elm.css({
                    top: scope.gkPosition.top + 'px',
                    left: scope.gkPosition.left + 'px'
                });
                return false;
            }
            function mouseup() {
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
            }

            // scroll event
            elm.bind("DOMMouseScroll mousewheel onmousewheel", function (event) {

                // cross-browser wheel delta
                var event = window.event || event; // old IE support
                var delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));

                // Prevent the entire page from scrolling up or down
                // for IE
                event.returnValue = false;
                // for Chrome and Firefox
                if (event.preventDefault) {
                    event.preventDefault();
                }

                // always rescale using the original proportions to avoid creep
                var initialWidth = elm.prop('naturalWidth');
                var initialHeight = elm.prop('naturalHeight');

                var step = delta * 2;
                var currHeight = elm.prop('height'), currWidth = elm.prop('width');
                
                scope.gkPosition.height = currHeight + 2 * step;
                scope.gkPosition.width = scope.gkPosition.height * initialWidth / initialHeight;
                scope.gkPosition.top = elm.prop('offsetTop') - step;
                scope.gkPosition.left = elm.prop('offsetLeft') - (scope.gkPosition.width - currWidth) / 2;

                elm.css({
                    top: scope.gkPosition.top + 'px',
                    left: scope.gkPosition.left + 'px',
                    height: scope.gkPosition.height + 'px',
                    width: scope.gkPosition.width + 'px'
                });
                return false;
            });
        }
    };
}]);