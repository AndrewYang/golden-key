app.directive('gkSprite', function($http) {
    return {
        restrict: 'E',
        template: "<div ng-style=\"{'background-size': (100 / ui.sprites[name].w) + '% ' + (100 / ui.sprites[name].h) + '%'," + 
                " 'background-position': (100 * ui.sprites[name].x) + '% ' + (100 * ui.sprites[name].y) + '%'," + 
                " 'padding-bottom': height }\"></div>",
        scope: {
            ui: '=',
            name: '=',
            ar: '=',
        },
        link: function(scope, element, attrs) {
            
            if (scope.ar == undefined) {
                scope.ar = 1;
            }
            scope.height = (100 * scope.ar) + '%';
        }
    }
});