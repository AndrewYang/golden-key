app.directive('gkDetailListingPanel', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'listingDetail.html',
        scope: {
            acc: '=',
            ui: '=',
            gui: '=',
            listing: '=',
            shown: '=',
            methods: '='
        },
        link: function(scope, element, attrs) {

            scope.$watch('[ui.en, listing.description]', function (newVal, oldVal) {
                if (newVal != oldVal) {
                    scope.changeLayout();
                }
            }, true);

            scope.changeLayout = function () {
                if (scope.ui.en && scope.listing != undefined && scope.listing.description != undefined && scope.listing.description.length > 0) {
                    scope.layout = 1;
                }
                else {
                    scope.layout = 2;
                }
            }
            scope.changeLayout();

            scope.inspectionTimes = [];
            scope.$watch('listing', function (newVal) { 
                if (newVal != undefined && scope.shown) {
                    scope.sortFeatures(newVal);

                    scope.inspectionTimes = [];
                    for (var i = 0; i < newVal.inspections.length; i++) {

                        var ins = newVal.inspections[i];

                        // check if acc.inspectionReminders contains this inspection
                        var found = false;
                        if (scope.acc != undefined) {
                            for (var a = 0; a < scope.acc.inspectionReminders.length; a++) {
                                var ir = scope.acc.inspectionReminders[a];
                                if (ir.lid == scope.listing.id && ir.start == ins.start) {
                                    found = true;
                                    break;
                                }
                            }
                        }

                        scope.inspectionTimes.push(getInspectionTimeDisplayObject(ins, found));
                    }
                }
            });
            function getInspectionTimeDisplayObject (ins, reminderSet) {
                return {
                    en: formatDateTimeInterval(ins.start, ins.end, 'en-AU', true),
                    ch: formatDateTimeInterval(ins.start, ins.end, 'zh-Hans-CN', true),
                    start: ins.start,
                    end: ins.end,
                    reminderSet: reminderSet
                };
            }

            scope.sortFeatures = function (listing) {

                console.log('recalculating features');

                if (listing == undefined) return;
                if (listing.sorted != undefined) return;    // already calculated

                listing.sorted = {
                    mainFeatures: [],
                    roomFeatures: [],
                    locationFeatures: [],
                    extraFeatures: []
                };

                for (var i = 0; i < listing.features.length; i++) {
                    var f = listing.features[i];
                        if (f.cat == 'Features') {
                            listing.sorted.mainFeatures.push(f);
                        }
                        else if (f.cat == 'Location') {
                            listing.sorted.locationFeatures.push(f);
                        }
                        else if (f.cat == 'Rooms') {
                            listing.sorted.roomFeatures.push(f);
                        }
                        else if (f.cat == 'Extras') {
                            listing.sorted.extraFeatures.push(f);
                        }
                }
            }

            scope.toggleInspectionReminder = function (inspection) {
                
                if (scope.listing == undefined) {
                    return;
                }

                // login required
                if (scope.acc == undefined) {
                    scope.ui.loginView = 'login';
                    scope.ui.login.customMsg = {
                        en: 'Please log in to continue.',
                        ch: '请登录访问更多内容。'
                    }
                    scope.ui.topElement = 'login';
                    return;
                }

                if (inspection.reminderSet) {
                    scope.removeInspectionReminder(inspection);
                }
                else {
                    scope.addInspectionReminder(inspection);
                }
            }
            scope.addInspectionReminder = function (inspection) {

                $http.post('/inspectionreminders/add', {
                    aid: scope.acc.id,
                    lid: scope.listing.id,
                    start: inspection.start,
                    end: inspection.end
                }).then(
                    function (res) {
                        if (res.data.success) {
                            scope.ui.addNotif({
                                en: 'Reminder set! We will email you 24 hours before the inspection time.',
                                ch: '提醒套装！我们会在检查时间前24小时通过电子邮件通知您。',
                                icon: 'tick'
                            });

                            var ir = res.data.result;
                            if (scope.acc != undefined && scope.acc.inspectionReminders != undefined) {
                                scope.acc.inspectionReminders.push(ir);
                            }
                            
                            for (var i = 0; i < scope.inspectionTimes.length; i++) {
                                if (ir.start == scope.inspectionTimes[i].start) {
                                    scope.inspectionTimes[i].reminderSet = true;
                                }
                            }
                        }
                    },
                    function error (err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                        console.log(err);
                    }
                );
            }
            scope.removeInspectionReminder = function (inspection) {
                
                $http.post('/inspectionreminders/remove', {
                    aid: scope.acc.id,
                    lid: scope.listing.id,
                    start: inspection.start
                }).then(
                    function (res) {
                        if (res.data.success) {
                            scope.ui.addNotif({
                                en: 'Inspection reminder removed.',
                                ch: '检查提醒已被删除。',
                                icon: 'tick'
                            });

                            var ir = res.data.result;

                            // Remove from acc.inspectionReminders 
                            if (scope.acc != undefined && scope.acc.inspectionReminders != undefined) {
                                for (var i = 0; i < scope.acc.inspectionReminders.length; i++) {
                                    if (ir.lid == scope.acc.inspectionReminders[i].lid && ir.start == scope.acc.inspectionReminders[i].start) {
                                        scope.acc.inspectionReminders.splice(i, 1);
                                        break;
                                    }
                                }
                            }

                            // Remove from scope.inspectionTimes
                            for (var i = 0; i < scope.inspectionTimes.length; i++) {
                                if (ir.start == scope.inspectionTimes[i].start) {
                                    scope.inspectionTimes[i].reminderSet = false;
                                }
                            }

                            console.log('inspection times: ' + JSON.stringify(scope.inspectionTimes));
                            console.log('inspection reminders: ' + JSON.stringify(scope.acc.inspectionReminders));
                        }
                    },
                    function error (err) {
                        if (err.status == 401) {
                            scope.ui.onInactivityLogout();
                            return;
                        }
                        console.log(err);
                    }
                );
            }
        }
    }
});
