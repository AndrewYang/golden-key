app.directive('gkChart', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'chart.html',
        scope: {
            type: '=',
            data: '='
        },
        link: function(scope, element, attrs) {
            
            angular.element(function () {

                scope.canvas = findCanvas(element[0].childNodes[0].childNodes);
                scope.parent = element.parent()[0];
                if (scope.canvas == undefined) {
                    return;
                }
                
                scope.canvas.height = scope.parent.clientHeight;
                scope.canvas.width = scope.parent.clientWidth;

                var ctx = scope.canvas.getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                        datasets: [
                            {
                                label: '# views per week',
                                data: scope.data,
                                backgroundColor: 'rgba(0, 51, 51, 0.2)',
                                borderColor: 'rgba(0, 51, 51, 1)',
                                borderWidth: 1
                            }
                        ]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });
        }
    }
});

function findCanvas (childNodes) {
    for (var i = 0; i < childNodes.length; i++) {
        var child = childNodes[i];
        if (child.nodeName == 'CANVAS') {
            return child;
        }
    }
    return undefined;
}

function getWeeks(time) {
    
}
function cumulative (data) {
    var cumulative = [];
    var cum = 0;
    for (var i = 0; i < data.length; i++) {
        cum += data[i];
        cumulative.push(cum);
    }
    return cum;
}