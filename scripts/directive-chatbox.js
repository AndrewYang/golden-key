app.directive('gkChatbox', function($http) {
    return {
        restrict: 'E',
        templateUrl: 'chatbox.html',
        link: function(scope, element, attrs) {

            scope.postMessage = function (msg) {

                // check to make sure that the listing is properly set
                if (scope.ui.chatbox.listing == null || scope.ui.chatbox.listing.id == null) {
                    scope.chatbox.msg = {
                        en: 'An error has occurred. Please refresh the page and try again.',
                        ch: '发生了错误。请刷新页面然后重试。'
                    }
                    return;
                }

                if (scope.acc == undefined || scope.acc.users.length == 0) {
                    if (msg.email == null) {
                        scope.chatbox.msg = {
                            en: 'Please enter your email address',
                            ch: '请输入您的电子邮件地址'
                        }
                        return;
                    }
                }
                else {
                    if (msg.user == null) {
                        scope.chatbox.msg = {
                            en: 'Please choose the user sending the message.',
                            ch: '请选择发送邮件的用户。'
                        }
                        return;
                    }
                }
                if (msg.subject == null) {
                    scope.chatbox.msg = {
                        en: 'Please select a subject category',
                        ch: '请选择主题类别'
                    }
                    return;
                }
                if (msg.text == null) {
                    scope.chatbox.msg = {
                        en: 'Please write your message',
                        ch: '请写下你的信息'
                    }
                    return;
                }

                $http.post('/chatbox', {
                    lid: scope.ui.chatbox.listing.id, 
                    aid: scope.acc == null ? null : scope.acc.id,
                    uid: msg.user, 
                    e: msg.email, 
                    s: msg.subject, 
                    t: msg.text
                }).then(
                    function success(res) {
                        console.log(res);
                        // get rid of the chatbox
                        scope.ui.chatbox.listing = null;
                        scope.ui.addNotif({
                            en: 'Message sent! The agent will reply to via email.',
                            ch: '消息已发送！代理商将回复您的电子邮件地址。',
                            icon: 'tick'
                        })
                    },
                    function error(err) {
                        scope.ui.addNotif({
                            en: 'Unable to send message at this time. Please try again later.',
                            ch: '目前无法发送消息。请稍后再试。',
                            icon: 'em'
                        })
                    }
                )
            }
        }
    }
});