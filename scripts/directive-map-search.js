app.directive('gkMapSearch', function($http, $compile) {
    return {
        restrict: 'E',
        templateUrl: 'mapsearch.html',

        scope: {
            ui: '=',
            methods: '='
        },

        link: function(scope, element, attrs) {

            scope.map = new google.maps.Map(document.getElementById('mapSearchMain'), {
                zoom: 5,
                center: { lat: -25.363, lng: 131.044 }  // aus
            });
            scope.infoWindow = new google.maps.InfoWindow();
            google.maps.event.addListener(scope.infoWindow,'closeclick', function () {
                scope.selectedListing = undefined;
            });
            
            scope.markers = [];

            var mapTimer = undefined;
            scope.map.addListener('bounds_changed', function() {

                // doing to to avoid too many queries
                clearTimeout(mapTimer);
                mapTimer = setTimeout(function () {

                    var mapBounds = scope.getMapBounds();
                    $http.post('/mapsearch', mapBounds).then(
                        function success (res) {
                            
                            console.log(res.data);

                            // Remove all existing markers
                            for (var i = scope.markers.length - 1; i >= 0; i--) {
                                var m = scope.markers[i];
                                // Don't remove the selected listing, if it exists
                                if (scope.selectedListing != undefined && m.listing.id == scope.selectedListing.id) {
                                    continue;
                                }
                                m.setMap(null);
                                scope.markers.splice(i, 1);
                            }

                            var locations = res.data.coords;
                            for (var i = 0; i < locations.length; i++) {  

                                var l = locations[i];

                                // make sure that it doesn't already exist
                                if (scope.selectedListing != undefined) {
                                    if (l.id == scope.selectedListing.id) continue;
                                }

                                // Generate map object
                                var marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(l.address.lat, l.address.lng),
                                    map: scope.map,
                                    listing: l
                                });
                                
                                marker.addListener('click', function () {
                                    scope.setInfoWindow(this);
                                });
                                scope.markers.push(marker);
                            }

                            
                        },
                        function error (err) {
                            console.log(err);
                        }
                    )
                }, 600);
            });
            scope.setInfoWindow = function (marker) {
                scope.infoWindow.close();

                var l = marker.listing;

                // Calculate address
                var address = l.address.street + ', ' + l.address.suburb;
                if (address.startsWith(',')) address = 'Address withheld';
                if (address.endsWith(', ')) address = address.substring(0, address.indexOf(','));

                var html = '<div style="width:30em"><h4 class="gk-nopadding">' + address + '</h4><div>' + (scope.ui.en ? l.price.en : l.price.ch) + '</div>';
                html += '<img class="map-iw-img" src="/images/' + l.images[0].i + '">';

                if (l.features != undefined) {
                    var fs = l.features;
                    for (var f = 0; f < fs.length; f++) {
                        html += '<div class="map-iw-ft">' + (scope.ui.en ? fs[f].en : fs[f].ch) + '</div>';
                    }
                }
                html += '<button class="gk-green-btn" ng-click="searchListing(selectedListing)">{{ ui.en ? "View details" : "查看详情" }}</button></div>';

                scope.selectedListing = l;
                var compiledHtml = $compile(html)(scope)[0];

                scope.infoWindow.setContent(compiledHtml);
                scope.infoWindow.open(scope.map, marker);
            }

            scope.searchListing = function (listing) {

                // when we search for a particular listing, we use the listingId field and also include some other query params 
                // So that we end up with some other similar properties

                var mapBounds = scope.getMapBounds();

                // Use the default
                var searchParams = {
                    ltypes: ['Sell'],
                    from: 0,
                    extrafrom: 0,
                    orderby: 'Most relevant',
                    listingid: listing.id,
                    minlat: mapBounds.b,
                    maxlat: mapBounds.t,
                    minlng: mapBounds.r,
                    maxlng: mapBounds.l
                };

                console.log(searchParams);
                $http.post('/search', searchParams).then(
                    function success (res) {
                        scope.ui.displayListings(res.data, true);
                        scope.ui.currMainDisplay = 'listings';

                        scope.ui.currSelectedPanelId = 0;
                        scope.ui.currDetailedPanelId = 0;
                        scope.ui.listingDataLoaded = true;
                    },
                    function error (err) {
                        console.log(err);
                    }
                )
            }
            scope.search = function () {
                scope.searching = true;
                $http.post('/mapsearchlistings', scope.getMapBounds()).then(
                    function success (res) {
                        scope.ui.displayListings(res.data, true);
                        scope.searching = false;
                        scope.ui.currMainDisplay = 'listings';
                    },
                    function error (err) {
                        console.log(err);
                        scope.searching = false;
                    }
                )
            }
            scope.getMapBounds = function () {
                var bounds =  scope.map.getBounds();
                var ne = bounds.getNorthEast();
                var sw = bounds.getSouthWest();

                return {
                    t: ne.lat(),
                    l: ne.lng(),
                    r: sw.lng(),
                    b: sw.lat()
                };
            }
        }
    }
});